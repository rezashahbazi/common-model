/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * Generic GRDD  Exception
 * @author rezashahbazi
 * @since October 2012
 */
@SuppressWarnings("serial")
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonIgnoreProperties({"stackTrace","suppressed","localizedMessage"})
public class GrddException extends Exception {
	private String message;
	private String code;
	
	
	public GrddException(String message) {
		super(message);
		this.message = message;
	}
	public GrddException(String message, String code) {
		super(message);
		this.message = message;
		this.code= code;
	}
	public GrddException(Throwable e) {
		super(e);		
	}
	public GrddException(String message,Throwable e) {
		//is.message = message;
		super(message,e);		
		this.message=message;
	}
	public String getMessage() {
		return message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
