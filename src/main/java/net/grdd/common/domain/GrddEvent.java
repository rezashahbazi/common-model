/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */

package net.grdd.common.domain;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
@JsonSerialize(include = Inclusion.NON_NULL)
public class GrddEvent
	implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String src;
	private Boolean respStatus;
	private String category;
	private String description;
	private Date timestamp;
	
	public GrddEvent()
	{
		this.timestamp = new Date();
	}

	public GrddEvent(Boolean respStatus, String category, String desc)
	{
		this();
		this.respStatus = respStatus;
		this.category = category;
		this.description = desc;
	}

	public String getSrc()
	{
		return src;
	}
	public void setSrc(String src)
	{
		this.src = src;
	}
	public String getCategory()
	{
		return category;
	}
	public void setCategory(String category)
	{
		this.category = category;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getTimestamp()
	{
		return timestamp;
	}
	public void setTimestamp(Date timestamp)
	{
		this.timestamp = timestamp;
	}
	public Boolean getRespStatus()
	{
		return respStatus;
	}
	public void setRespStatus(Boolean status)
	{
		this.respStatus = status;
	}

	@Override
	public String toString()
	{
		return "GrddEvent [src=" + src + ", category=" + category +
			", description=" + description + ", timestamp=" + timestamp +
			", respStatus=" + respStatus + "]";
	}
}
