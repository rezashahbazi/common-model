package net.grdd.common.domain;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.map.ser.std.SerializerBase;

@JacksonStdImpl
public class DateSerializer extends SerializerBase<Date> {
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	public final static DateSerializer instance = new DateSerializer();

    public DateSerializer() { super(java.util.Date.class); }

    @Override
    public void serialize(java.util.Date date, JsonGenerator jgen, SerializerProvider provider)
        throws IOException, JsonGenerationException
    {
    	jgen.writeString(sdf.format(date));
    }

    @Override
    public JsonNode getSchema(SerializerProvider provider, Type typeHint)
    {
        //todo: (ryan) add a format for the date in the schema?
        return createSchemaNode("string", true);
    }


}
