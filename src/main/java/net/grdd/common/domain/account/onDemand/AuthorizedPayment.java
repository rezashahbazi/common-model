/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.account.onDemand;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import net.grdd.common.domain.BaseModel;

/**
 * @author mehez
 * @since Dec 2, 2014
 */

/* The Payment domain object cannot be used for this purpose, as the 
 * attribute of this objects are different and pertaining to storing transaction information 
 * from the Payment Gateways (e.g Blue Pay, Stripe etc).
 * Third party payment gateways are used only in case of onDemand Reservations
 */
@Document(collection="authorizedPayment")
@CompoundIndex(name = "grdIdResNo_Index", def = "{'griddID': 1, 'resNo': 1}", unique = true)
public class AuthorizedPayment  extends BaseModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5240592335217072070L;
	
	// The attributes GriddId, user will be inherited and used from the BaseModel
	
	
	@Indexed
	private String resNo;

	private BigDecimal estimatedAmount;
	
	private BigDecimal authorizationAmount;
	
//	private String ccRegistrationIdentifier; // Identifier that tracks a registered card with the payment gateway
	
	private String ccId; // the _id field from the Passenger Credit Card Collection
	
	private String gatewayAuthorizationTransactionId ; // The authorization transaction id with the payment gateway 
	
//	@Transient
	private PaymentResponse paymentResponse;

	public AuthorizedPayment() {
		super();
	return;	
	}

	public String getResNo() {
		return resNo;
	}

	public void setResNo(String resNo) {
		this.resNo = resNo;
	}

	public BigDecimal getEstimatedAmount() {
		return estimatedAmount;
	}

	public void setEstimatedAmount(BigDecimal estimatedAmount) {
		this.estimatedAmount = estimatedAmount;
	}

/*	public String getCcRegistrationIdentifier() {
		return ccRegistrationIdentifier;
	}

	public void setCcRegistrationIdentifier(String ccRegistrationIdentifier) {
		this.ccRegistrationIdentifier = ccRegistrationIdentifier;
	} */

	public String getGatewayAuthorizationTransactionId() {
		return gatewayAuthorizationTransactionId;
	}

	public void setGatewayAuthorizationTransactionId(String gatewayTransactionId) {
		this.gatewayAuthorizationTransactionId = gatewayTransactionId;
	}

//	@Transient
	@JsonIgnore
	public PaymentResponse getPaymentResponse() {
		return paymentResponse;
	}

	public void setPaymentResponse(PaymentResponse paymentResponse) {
		this.paymentResponse = paymentResponse;
	}

	public BigDecimal getAuthorizationAmount() {
		return authorizationAmount;
	}

	public void setAuthorizationAmount(BigDecimal authorizationAmount) {
		this.authorizationAmount = authorizationAmount;
	}

	public String getCcId() {
		return ccId;
	}

	public void setCcId(String ccId) {
		this.ccId = ccId;
	}


	
}