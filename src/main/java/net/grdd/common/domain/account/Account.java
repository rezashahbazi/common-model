/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.account;

import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.contact.Company;

/**
 * Account information about a client
 * 
 * @author rezashahbazi
 * @since Sep 14, 2011
 */
public class Account
	extends BaseModel
{

	private static final long serialVersionUID = -4274859382431117756L;

	// fixed account number
	private String accountNumber;

	// account name can be change
	private String accountName;

	private String department;

	private String callerName;

	private String callerNumber;

	private Company company;

	private String accountType;
	
	private String commentsForDriver;
	
	private String commentsForDispatcher;

	

	public String getAccountNumber()
	{
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	public String getDepartment()
	{
		return department;
	}

	public void setDepartment(String department)
	{
		this.department = department;
	}

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	public String getCallerName()
	{
		return callerName;
	}

	public void setCallerName(String callerName)
	{
		this.callerName = callerName;
	}

	public String getCallerNumber()
	{
		return callerNumber;
	}

	public void setCallerNumber(String callerNumber)
	{
		this.callerNumber = callerNumber;
	}

	public String getAccountType()
	{
		return accountType;
	}

	public void setAccountType(String accountType)
	{
		this.accountType = accountType;
	}
	
	

	public String getCommentsForDriver() {
		return commentsForDriver;
	}

	public void setCommentsForDriver(String commentsForDriver) {
		this.commentsForDriver = commentsForDriver;
	}

	public String getCommentsForDispatcher() {
		return commentsForDispatcher;
	}

	public void setCommentsForDispatcher(String commentsForDispatcher) {
		this.commentsForDispatcher = commentsForDispatcher;
	}
	@XmlTransient
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString()
	{
		return "Account [accountNumber=" + accountNumber + ", accountName=" +
			accountName + ", department=" + department + ", callerName=" +
			callerName + ", callerNumber=" + callerNumber + ", client=" +
			company + ", accountType=" + accountType + "]";
	}

}
