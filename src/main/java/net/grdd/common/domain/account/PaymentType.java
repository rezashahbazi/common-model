/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.account;

import javax.xml.bind.annotation.XmlSeeAlso;

import net.grdd.common.domain.BaseModel;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

/**
 * @author rezashahbazi
 * @since Nov 16, 2011
 */
@XmlSeeAlso({PaymentCardType.class, PaymentBankType.class, PaymentDirectBillType.class})
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
	@JsonSubTypes.Type(name="PaymentCardType", value=PaymentCardType.class),
	@JsonSubTypes.Type(name="PaymentBankType", value=PaymentBankType.class),
	@JsonSubTypes.Type(name="PaymentDirectBillType", value=PaymentDirectBillType.class)
})
public abstract class PaymentType extends BaseModel{
	
	private static final long serialVersionUID = 7887645941562574442L;
	
	

}
