/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.account;

import net.grdd.common.domain.Address;

/**
 * @author rezashahbazi
 * @since Nov 16, 2011
 */
public class PaymentDirectBillType  extends PaymentType{
	private String companyName;
    private Address address;
    private String email;
    private String telephone;
    
    private String directBillID;
    private String billingNumber;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getDirectBillID() {
		return directBillID;
	}
	public void setDirectBillID(String directBillID) {
		this.directBillID = directBillID;
	}
	public String getBillingNumber() {
		return billingNumber;
	}
	public void setBillingNumber(String billingNumber) {
		this.billingNumber = billingNumber;
	}
    
    
}
