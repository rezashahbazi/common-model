/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.account;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rezashahbazi
 * @since Nov 16, 2011
 */
@XmlRootElement
public class PaymentBankType extends PaymentType {

	private static final long serialVersionUID = 817303745997756452L;
	private String bankAcctName;
    private String bankID;
    private String acct;
    private String bankAcctNumber;
    private Boolean checksAcceptedInd;
    private String checkNumber;
	public String getBankAcctName() {
		return bankAcctName;
	}
	public void setBankAcctName(String bankAcctName) {
		this.bankAcctName = bankAcctName;
	}
	public String getBankID() {
		return bankID;
	}
	public void setBankID(String bankID) {
		this.bankID = bankID;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getBankAcctNumber() {
		return bankAcctNumber;
	}
	public void setBankAcctNumber(String bankAcctNumber) {
		this.bankAcctNumber = bankAcctNumber;
	}
	public Boolean getChecksAcceptedInd() {
		return checksAcceptedInd;
	}
	public void setChecksAcceptedInd(Boolean checksAcceptedInd) {
		this.checksAcceptedInd = checksAcceptedInd;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
    
}
