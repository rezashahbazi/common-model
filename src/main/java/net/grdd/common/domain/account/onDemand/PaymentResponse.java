package net.grdd.common.domain.account.onDemand;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PaymentResponse {
	private String status;
	private String message;
	
	public static final String SUCCESS = "Success";  
	public static final String FAILURE = "Fail";
	
	public static final String APPROVED = "Approved";
	public static final String DECLINED = "Declined";
	
	
	public PaymentResponse(){
		
	}
	
	public PaymentResponse(String status, String message){
		super();
		this.status = status;
		this.message = message;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
