/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.account.onDemand;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import net.grdd.common.domain.BaseModel;

/**
 * @author mehez
 * @since Dec 2, 2014
 */

/* The Payment domain object cannot be used for this purpose, as the 
 * attribute of this objects are different and pertaining to storing transaction information 
 * from the Payment Gateways (e.g Blue Pay, Stripe etc).
 * Third party payment gateways are used only in case of onDemand Reservations
 * CollectedPayment tracks the actual funds trasferred after the authorization step
 */
@Document(collection="collectedPayment")
@CompoundIndex(name = "grdIdResNo_Index", def = "{'griddID': 1, 'resNo': 1}", unique = true)
public class CollectedPayment  extends BaseModel{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7687090730601929252L;

	// The attributes GriddId, user will be inherited and used from the BaseModel
	
	@Indexed
	private String resNo;

	private BigDecimal actualTotalAmount;
	
	private String gatewayAuthorizationTransactionId ; // the gatewayAuthorizationTransactionId from authorized Collection 

	private String gatewayCollectedTransactionId ; // the gatewayAuthorizationTransactionId from authorized Collection 

	
//	@Transient
	private PaymentResponse paymentResponse;

	public CollectedPayment() {
		super();
	return;	
	}

	public String getResNo() {
		return resNo;
	}

	public void setResNo(String resNo) {
		this.resNo = resNo;
	}


	public String getGatewayAuthorizationTransactionId() {
		return gatewayAuthorizationTransactionId;
	}

	public void setGatewayAuthorizationTransactionId(String gatewayTransactionId) {
		this.gatewayAuthorizationTransactionId = gatewayTransactionId;
	}

//	@Transient
	@JsonIgnore
	public PaymentResponse getPaymentResponse() {
		return paymentResponse;
	}

	public void setPaymentResponse(PaymentResponse paymentResponse) {
		this.paymentResponse = paymentResponse;
	}

	public String getGatewayCollectedTransactionId() {
		return gatewayCollectedTransactionId;
	}

	public void setGatewayCollectedTransactionId(
			String gatewayCollectedTransactionId) {
		this.gatewayCollectedTransactionId = gatewayCollectedTransactionId;
	}

	public BigDecimal getActualTotalAmount() {
		return actualTotalAmount;
	}

	public void setActualTotalAmount(BigDecimal actualTotalAmount) {
		this.actualTotalAmount = actualTotalAmount;
	}


	
}