/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


/**
 * @author reza shahbazi
 * @since Sep 12, 2011
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonSerialize(include = Inclusion.NON_NULL)
//@DiscriminatorColumn(name = "address_type", discriminatorType = DiscriminatorType.STRING)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name="contact_address")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address implements Serializable{
	
	@Id 
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long gid;

	private static final long serialVersionUID = -6473432420477833673L;
	
	// room or Apart #
	private String roomNumber;
	
	// full address entered by user
	private String address;
	// street
	private String address1;
	// suite number
	private String address2;
	// this can be used for Cross Stress, Address On Location (e.g. CBS Studios) etc
	private String address3;
	private String city;
	private String state;	
	private String zipCode;
	private String country;
	private String timeZone;
	
	// use lng
	@Deprecated
	private BigDecimal lon;
	private BigDecimal lng;
	private BigDecimal lat;
	
	// home, office, etc.
	private String type;
	
	@Column
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	@Column
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Column
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	@Column
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	@Column
	public BigDecimal getLon() {
		return lon;
	}
	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}
	@Column
	public BigDecimal getLat() {
		return lat;
	}
	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}
	
	public BigDecimal getLng() {
		return lng;
	}
	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}
	@Column
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	
	public Long getGid() {
		return gid;
	}
	public void setGid(Long gid) {
		this.gid = gid;
	}
	
	
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Transient
	@JsonIgnore
	//@XmlTransient
	public boolean hasStandardAddress(){
		return !isXAL() && StringUtils.isNotBlank(address1) || StringUtils.isNotBlank(address2) || StringUtils.isNotBlank(city)|| StringUtils.isNotBlank(zipCode);
	}
	/**
	 * xAL (eXtensible Address Language)
	 * @return
	 */
	@Transient
	@JsonIgnore
	//@XmlTransient
	public boolean isXAL(){
		return StringUtils.isBlank(address1) &&   StringUtils.isNotBlank(address);					
	}
	
	public void updateStandard(){
		this.address = getShortAddress();
	}
	
	@Transient
	@JsonIgnore
	//@XmlTransient
	public String getShortAddress(){
		/*
		 * address2 OR terminal is missing 
		 */
		if (isXAL()){
			return (StringUtils.isNotBlank(address2)? address2+" ":"") +address;
		}
		return  ""+ (StringUtils.isNotBlank(address1)?  address1 + " ":"") + (StringUtils.isNotBlank(address2)? " "+ address2:"")+
				 (StringUtils.isNotBlank(city)? " ," + city:"")+(StringUtils.isNotBlank(zipCode)? " ," + zipCode:"");
	}
	
	@Override
	public String toString() {
		return "Address [address1=" + address1 + ", address2=" + address2
				+ ", address3=" + address3 + ", city=" + city + ", state="
				+ state + ", zipCode=" + zipCode + ", country=" + country
				+ ", lon=" + lon + ", lat=" + lat + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result
				+ ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result
				+ ((address3 == null) ? 0 : address3.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((lat == null) ? 0 : lat.hashCode());
		result = prime * result + ((lon == null) ? 0 : lon.hashCode());
		result = prime * result
				+ ((roomNumber == null) ? 0 : roomNumber.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result
				+ ((timeZone == null) ? 0 : timeZone.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (address2 == null) {
			if (other.address2 != null)
				return false;
		} else if (!address2.equals(other.address2))
			return false;
		if (address3 == null) {
			if (other.address3 != null)
				return false;
		} else if (!address3.equals(other.address3))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (lat == null) {
			if (other.lat != null)
				return false;
		} else if (!lat.equals(other.lat))
			return false;
		if (lon == null) {
			if (other.lon != null)
				return false;
		} else if (!lon.equals(other.lon))
			return false;
		if (roomNumber == null) {
			if (other.roomNumber != null)
				return false;
		} else if (!roomNumber.equals(other.roomNumber))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (timeZone == null) {
			if (other.timeZone != null)
				return false;
		} else if (!timeZone.equals(other.timeZone))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}
	//@Override
	public int compareTo(Address o) {
		//new CompareToBuilder();
		/*return new CompareToBuilder()	      
	       .append(this.address, o.address)
	       .append(this.address2, o.address2)
	       .append(this.city, o.city)
	       .append(this.zipCode, o.zipCode)
	       .toComparison();*/
		return CompareToBuilder.reflectionCompare(this,o ,"gid", "locationType");
	}
	
	
}
