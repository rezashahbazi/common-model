/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;

/**
 * High level class to represent Operator, Client, Agency, etc.
 * ALTER TABLE  `configuration` CHANGE  `configuration`  `configuration` VARCHAR( 10000 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL

 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "contact_type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "contact_company",  uniqueConstraints={ @UniqueConstraint(columnNames={"griddID"})})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "contactType")
@JsonSubTypes({ @Type(value = Operator.class, name = "Operator"),
		@Type(value = Client.class, name = "Client") })
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Company extends AbstractContact {
	private static final long serialVersionUID = 3216131966038788911L;
	public Company() {	
		if (this.contactType==null){
			this.contactType =  this.getClass().getSimpleName();
		}
	}

	public Company(String name) {
		this();
		this.name = name;
	}

	

	// Lack of @DiscriminatorColumn for InheritanceType.JOINED
	@Index(name = "co_type_idx")
	@Column(nullable = false, length = 10)
	@Size(min=4,max=150)
	private String contactType ;
	
	// Company (Agent/Affiliate) Code
	private String code;

	@Index(name = "co_name_idx")
	private String name;
	// web site
	private String website;
	
	

	@Column(name = "fax_number", length = 50)
	private String faxNumber;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "configuration", joinColumns = @JoinColumn(name = "configuration_id"))
	@MapKeyColumn(name = "configuration_key")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Map<String, String> configuration;



	@Valid
	@OneToOne
	@JoinColumn(name = "license_id")
	BusinessLicense license;

	@Valid
	@OneToOne
	@JoinColumn(name = "insurance_id")
	BusinessInsurance insurance;

	private String taxId;

	// overview and background
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "profile_id_pk")
	private Profile profile;
	
	//@OneToMany(fetch=FetchType.LAZY)
	/*@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinTable(name = "company_contacts", joinColumns = @JoinColumn(name = "company_id"), inverseJoinColumns = @JoinColumn(name = "contact_id"))
	@MapKeyJoinColumn(name = "ROLE")
	Map<String, Contact> contacts;
	 */
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL,orphanRemoval=true,mappedBy="company")
	//@JoinColumn(name = "company_id")
	private Set<Contact> contacts;
	
	@JsonIgnore
	@Transient
	@XmlTransient
	String status;
	
	
	@NotNull
	@Column(unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BusinessLicense getLicense() {
		return license;
	}

	public void setLicense(BusinessLicense license) {
		this.license = license;
	}

	public BusinessInsurance getInsurance() {
		return insurance;
	}

	public void setInsurance(BusinessInsurance insurance) {
		this.insurance = insurance;
	}

	

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	



	

	@Override
	public String toString() {
		return "Company [contactType=" + contactType + ", name=" + name
				+ ", email=" + getEmail() + ", active=" + super.getActive() + "]";
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

	public void addConfiguration(String key, String configuration) {
		if (this.configuration == null) {
			this.configuration = new HashMap<String, String>();
		}
		this.configuration.put(key, configuration);
	}

	public String getConfiguration(String key) {
		if (this.configuration == null) {
			return null;
		}
		return this.configuration.get(key);
	}

	/**
	 * If adapter is enable then return the url from the configuration
	 * 
	 * @return
	 */
	public String adapterURL() {
		String url = getConfiguration("adapter_url");
		if (url != null
				&& "true".equalsIgnoreCase(getConfiguration("adapter_active"))) {
			return url;
		}
		return null;
	}

	public String getContactType() {
		this.getClass().getName();

		return contactType;
	}
	
	/**
	 * Ideally should not be able to set the contactType, but for signup page, we need to set the contactType to create either operator or client, etc.
	 * @param contactType
	 */
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	@JsonIgnore
	@XmlTransient
	public Set<Contact> getContacts() {
		return contacts;
	}
	@JsonIgnore
	@XmlTransient
	public Contact getContactByRole(String role) {
		if( contacts == null || role == null) return null;
		for (Contact contact : contacts) {
			if (role.endsWith(contact.getRole())){
				return contact;
			}
		}
		return null;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public void addContact(Contact contact) {
		if (contact == null){
			return;			
		}
		if (this.contacts == null){
			this.contacts = new HashSet<Contact>();
		}
		
		contacts.add(contact);
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getActive() == null) ? 0 : getActive().hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((contactType == null) ? 0 : contactType.hashCode());
		result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
		result = prime * result
				+ ((faxNumber == null) ? 0 : faxNumber.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		
		
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + ((taxId == null) ? 0 : taxId.hashCode());
		result = prime * result + ((website == null) ? 0 : website.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (getActive() == null) {
			if (other.getActive() != null)
				return false;
		} else if (!getActive().equals(other.getActive()))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (contactType == null) {
			if (other.contactType != null)
				return false;
		} else if (!contactType.equals(other.contactType))
			return false;
		if (getEmail() == null) {
			if (other.getEmail() != null)
				return false;
		} else if (!getEmail().equals(other.getEmail()))
			return false;
		if (faxNumber == null) {
			if (other.faxNumber != null)
				return false;
		} else if (!faxNumber.equals(other.faxNumber))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
	
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (taxId == null) {
			if (other.taxId != null)
				return false;
		} else if (!taxId.equals(other.taxId))
			return false;
		if (website == null) {
			if (other.website != null)
				return false;
		} else if (!website.equals(other.website))
			return false;
		return true;
	}

	
	

}
