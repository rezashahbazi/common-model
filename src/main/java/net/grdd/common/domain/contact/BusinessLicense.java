/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * @author rezashahbazi
 * @since Oct 13, 2011
 */
@Entity
@Table(name="operator_license")
public class BusinessLicense extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	String licenseTitle;
	String licenseContent;
	Date startDate;
	Date endDateDate;
	String authority; //government
	public String getLicenseTitle() {
		return licenseTitle;
	}
	public void setLicenseTitle(String licenseTitle) {
		this.licenseTitle = licenseTitle;
	}
	public String getLicenseContent() {
		return licenseContent;
	}
	public void setLicenseContent(String licenseContent) {
		this.licenseContent = licenseContent;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getEndDateDate() {
		return endDateDate;
	}
	public void setEndDateDate(Date endDateDate) {
		this.endDateDate = endDateDate;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
}
