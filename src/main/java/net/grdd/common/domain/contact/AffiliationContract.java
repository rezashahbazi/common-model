/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.grdd.common.domain.BaseModel;

/**
 * @author rezashahbazi
 * @since Oct 13, 2011
 */
@Entity
@Table(name="operator_contract")
public class AffiliationContract extends BaseModel {
	// Document
	// 
}
