/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import net.grdd.common.domain.Address;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Document(collection="contact")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public class Person extends AbstractContact implements Comparable<Person> {

	
	private static final long serialVersionUID = 6244120631403931625L;
	private String firstName;
	private String middleName;
	private String alias;
	
	@NotNull
	private String lastName;
	
	
	
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_address_id")
	private List<Address> addresses;
	
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name ="contact_phone_id")
	private List<Phone> phones;
	
	
	
	@Transient
	private Map<String, String> preferences;
	
	
	public Person() {
		
	}
	public Person(String firstName, String lastName, String email) {		
		this.firstName = firstName;
		this.lastName = lastName;
		super.setEmail ( email);
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	@Transient
	@JsonIgnore
	//@XmlTransient
	public String getFullName() {
		return (StringUtils.isNotBlank(firstName)?firstName+ " ":"")+ StringUtils.trimToEmpty(lastName);
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + "]";
	}
	
	
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
	public void addPhone(Phone phone) {
		if (this.phones ==null){
			this.phones = new ArrayList<Phone>();
		}
		this.phones.add(phone);
	}
	
	public Phone getPhoneByType(String type){
		if (this.phones ==null || type==null){ return null;}
		for (Phone phone : this.phones) {
			if (type.equalsIgnoreCase(phone.type)){
				return phone;
			}
		}
		return null;
	}
	
	
	
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	public void addAddress(Address address) {
		
		if (this.addresses ==null){
			this.addresses = new ArrayList<Address>();
		}
		// override if the same address type exists
		if (address!=null && StringUtils.isNotBlank(address.getType())){
			List<Address> list = new ArrayList<Address>(); 
			for (Address exist : this.addresses) {
				if (!address.getType().equalsIgnoreCase(exist.getType())){
					list.add(exist);
				}else{
					// skipping
				}
				
			}
			list.add(address);
			this.addresses =  list;
		}else{
			this.addresses.add(address);
		}
	}
	
	
	@Transient
	@JsonIgnore
	//@XmlTransient
	public Phone getMobilePhone(){
		return getPhoneByType("MOBILE");
	}
	
	/**
	 * First try mobile phone, then phoneNumber
	 * @return
	 */
	@Transient
	@JsonIgnore
	//@XmlTransient
	public String getPrimaryPhone(){
		 Phone mobile = getPhoneByType("MOBILE");
		 if (mobile!=null && StringUtils.isNotBlank(mobile.number)){
			 return mobile.number;
		 }
		 return super.getPhoneNumber();
	}
	
	@Transient
	@JsonIgnore
	//@XmlTransient
	public Phone getFax(){
		return getPhoneByType("FAX");
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((middleName == null) ? 0 : middleName.hashCode());
	
		
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (super.getEmail() == null) {
			if (other.getEmail() != null)
				return false;
		} else if (!getEmail().equals(other.getEmail()))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		
		return true;
	}
	
	public Map<String, String> getPreferences() {
		return preferences;
	}
	public void setPreferences(Map<String, String> preferences) {
		this.preferences = preferences;
	}
	public void addPreference(String key, String configuration) {
		if (this.preferences == null) {
			this.preferences = new HashMap<String, String>();
		}
		this.preferences.put(key, configuration);
	}

	@Transient
	@JsonIgnore
	//@XmlTransient
	public String getPreference(String key) {
		if (this.preferences == null) {
			return null;
		}
		return this.preferences.get(key);
	}
	
	
	
	
	@Override
	public int compareTo(Person o) {
		return new CompareToBuilder()	      
	       .append(this.firstName, o.firstName)
	       .append(this.lastName, o.lastName)
	       .append(this.getEmail(), o.getEmail())
	       .toComparison();
	}
}
