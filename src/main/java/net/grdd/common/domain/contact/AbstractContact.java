package net.grdd.common.domain.contact;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import net.grdd.common.domain.Address;
import net.grdd.common.domain.BaseModel;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractContact extends BaseModel{

	@Pattern(regexp = "(^$|(([\\w\\-\\.]+)([\\w]+))@((([\\w\\-]+\\.)+)([a-zA-Z]{2,4})))")
    @Column(unique=true)
    @Indexed    
    @Id
	private String email;

	private String phoneNumber;
	
	private String defaultLangauge;
	private String defaultTimezone;

	
	@Valid
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Address primaryAddress;
	
	@org.hibernate.annotations.Type(type="yes_no")
	private Boolean active = true;
	
	private String accountNumber;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getPrimaryAddress() {
		return primaryAddress;
	}

	public void setPrimaryAddress(Address primaryAddress) {
		this.primaryAddress = primaryAddress;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getDefaultLangauge() {
		return defaultLangauge;
	}

	public void setDefaultLangauge(String defaultLangauge) {
		this.defaultLangauge = defaultLangauge;
	}

	public String getDefaultTimezone() {
		return defaultTimezone;
	}

	public void setDefaultTimezone(String defaultTimezone) {
		this.defaultTimezone = defaultTimezone;
	}
	
	
}
