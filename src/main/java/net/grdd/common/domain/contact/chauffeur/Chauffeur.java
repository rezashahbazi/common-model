/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact.chauffeur;

import net.grdd.common.domain.contact.Person;
import net.grdd.common.domain.contact.Phone;

import org.springframework.data.mongodb.core.mapping.Document;
/**
 * Chauffeur object
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
//@Entity
@Document(collection="chauffeur")
//@Table
public class Chauffeur extends Person{
	private static final long serialVersionUID = 2465743048138604102L;
	private String driverLicense;
	
	
	private String chauffeurId;
	
	
	public String getChauffeurId() {
		return chauffeurId;
	}
	public void setChauffeurId(String chauffeurId) {
		this.chauffeurId = chauffeurId;
	}
	
	public String getDriverLicense() {
		return driverLicense;
	}
	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}
	public void addPhone(String number,String value, Boolean visibleToPassenger) {
		Phone phone =new Phone(number, value);
		if (visibleToPassenger!=null){
			phone.addAttribute("visibleToPassenger", visibleToPassenger?"true":"false");
		}
		addPhone(phone);
	}
	
}
