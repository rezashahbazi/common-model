/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Represent a client (some times called account)
 * For example FOX 21 Centuries
 * 
 * alter table  contact_client drop column  account_number;
 * alter table  contact_client drop column  active;


 * @author rezashahbazi
 * @since Sep 14, 2011
 */
@Entity
@Table(name="contact_client")
public class Client extends Company{
	
	private static final long serialVersionUID = -2087664640206125522L;
	
	private String firstName;

	private String lastName;
	
	
	private String authorizedOperator;
	
	// public or private
	private String visibility = "private";

	public Client(String name) {
		super(name);
	}

	public Client() {
		
	}

	public String getAuthorizedOperator() {
		return authorizedOperator;
	}

	public void setAuthorizedOperator(String authorizedOperator) {
		this.authorizedOperator = authorizedOperator;
	}

	

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Client [firstName=" + firstName + ", lastName=" + lastName
				+ ", authorizedOperator=" + authorizedOperator + ", visibility="
				+ visibility + "]" + super.toString();
	}

	
	
}
