/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import net.grdd.common.domain.BaseModel;

/**
 * @author rezashahbazi
 * @since Oct 13, 2011
 */
@Entity
@Table(name="contact_profile")
public class Profile extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	private String logoUrl;
	private String iconUrl;
	
	@Column(length = 5000)
	private String missionStatement;
	
	@Column(length = 5000)
	private String aboutUs;
	private String fleetInfo;
	private String promotionTitle;
	private String promotionDeal;
	private String promotionDetails;
	private String promotionImageUrl;
	
	private String emailGeneral;
	private String emailReservations;
	private String emailBilling;
	private String emailIssues;
	private String phoneGeneral;
	private String phoneReservations;
	private String phoneBilling;	
	private String phoneIssues;
	
	
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getMissionStatement() {
		return missionStatement;
	}
	public void setMissionStatement(String missionStatement) {
		this.missionStatement = missionStatement;
	}
	public String getAboutUs() {
		return aboutUs;
	}
	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}
	public String getFleetInfo() {
		return fleetInfo;
	}
	public void setFleetInfo(String fleetInfo) {
		this.fleetInfo = fleetInfo;
	}
	public String getPromotionTitle() {
		return promotionTitle;
	}
	public void setPromotionTitle(String promotionTitle) {
		this.promotionTitle = promotionTitle;
	}
	public String getPromotionDeal() {
		return promotionDeal;
	}
	public void setPromotionDeal(String promotionDeal) {
		this.promotionDeal = promotionDeal;
	}
	public String getPromotionDetails() {
		return promotionDetails;
	}
	public void setPromotionDetails(String promotionDetails) {
		this.promotionDetails = promotionDetails;
	}
	
	public String getPromotionImageUrl() {
		return promotionImageUrl;
	}
	public void setPromotionImageUrl(String promotionImageUrl) {
		this.promotionImageUrl = promotionImageUrl;
	}
	public String getEmailGeneral() {
		return emailGeneral;
	}
	public void setEmailGeneral(String emailGeneral) {
		this.emailGeneral = emailGeneral;
	}
	public String getEmailReservations() {
		return emailReservations;
	}
	public void setEmailReservations(String emailReservations) {
		this.emailReservations = emailReservations;
	}
	public String getEmailBilling() {
		return emailBilling;
	}
	public void setEmailBilling(String emailBilling) {
		this.emailBilling = emailBilling;
	}
	public String getEmailIssues() {
		return emailIssues;
	}
	public void setEmailIssues(String emailIssues) {
		this.emailIssues = emailIssues;
	}
	public String getPhoneGeneral() {
		return phoneGeneral;
	}
	public void setPhoneGeneral(String phoneGeneral) {
		this.phoneGeneral = phoneGeneral;
	}
	public String getPhoneReservations() {
		return phoneReservations;
	}
	public void setPhoneReservations(String phoneReservations) {
		this.phoneReservations = phoneReservations;
	}
	public String getPhoneBilling() {
		return phoneBilling;
	}
	public void setPhoneBilling(String phoneBilling) {
		this.phoneBilling = phoneBilling;
	}
	public String getPhoneIssues() {
		return phoneIssues;
	}
	public void setPhoneIssues(String phoneIssues) {
		this.phoneIssues = phoneIssues;
	}
	
	@Override
	public String toString() {
		return "Profile [logoUrl=" + logoUrl + ", iconUrl=" + iconUrl
				+ ", missionStatement=" + missionStatement + ", aboutUs="
				+ aboutUs + ", fleetInfo=" + fleetInfo + ", promotionTitle="
				+ promotionTitle + ", promotionDeal=" + promotionDeal
				+ ", promotionDetails=" + promotionDetails
				+ ", promotionImageUrl=" + promotionImageUrl
				+ ", emailGeneral=" + emailGeneral + ", emailReservations="
				+ emailReservations + ", emailBilling=" + emailBilling
				+ ", emailIssues=" + emailIssues + ", phoneGeneral="
				+ phoneGeneral + ", phoneReservations=" + phoneReservations
				+ ", phoneBilling=" + phoneBilling + ", phoneIssues="
				+ phoneIssues + "]";
	}
	
	
	
	
	
}
