/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Passenger Profile 
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name="reservation_passenger")
public class Passenger extends Person {

	
	private static final long serialVersionUID = 8168244579150884680L;
	// passenger preferences
	//private String preferences;
	
	private String specialInstructions;
	
	private String commentsForDriver;
	
	private String commentsForDispatcher;
	
	private String imageURL;
	
	
	public String getSpecialInstructions() {
		return specialInstructions;
	}
	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	
	public String getCommentsForDriver() {
		return commentsForDriver;
	}
	public void setCommentsForDriver(String commentsForDriver) {
		this.commentsForDriver = commentsForDriver;
	}
	public String getCommentsForDispatcher() {
		return commentsForDispatcher;
	}
	public void setCommentsForDispatcher(String commentsForDispatcher) {
		this.commentsForDispatcher = commentsForDispatcher;
	}
	@Override
	public String toString() {
		return "Passenger ["+ super.toString() 
				+ ", specialInstructions=" + specialInstructions
				+ ", imageURL=" + imageURL 
				+ "]";
	}
	
	public void addPhone(String number,String value, Boolean sendNotification) {
		Phone phone =new Phone(number, value);
		if (sendNotification!=null){
			phone.addAttribute("sendNotification", sendNotification?"TRUE":"FALSE");
		}
		addPhone(phone);
	}

}
