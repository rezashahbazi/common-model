/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

/**
 * @author rezashahbazi
 * @since Oct 13, 2011
 */
@Entity
@Table(name="service_area")
public class ServiceArea implements Serializable , Comparable<ServiceArea> {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long gid;
	
	private static final long serialVersionUID = 1L;
	private String label; //Los Angeles Interntioal 
	private String name ; //lax
	
	private BigDecimal lon;
	private BigDecimal lat;
	private BigDecimal distance;
	private Integer serviceAreaRadius;// 10 miles
	
	public ServiceArea() {
		
	}
	public ServiceArea(String areaName, Integer areaRadius) {
		name = areaName;
		serviceAreaRadius = areaRadius;
	}
	//@NotNull
	//@Column(unique=true)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getServiceAreaRadius() {
		return serviceAreaRadius;
	}
	public void setServiceAreaRadius(Integer serviceAreaRadius) {
		this.serviceAreaRadius = serviceAreaRadius;
	}
	
	public BigDecimal getLon() {
		return lon;
	}
	public void setLon(BigDecimal lon) {
		this.lon = lon;
	}
	public BigDecimal getLat() {
		return lat;
	}
	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}
	@Index(name="sa_label")
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public Long getGid() {
		return gid;
	}
	public void setGid(Long gid) {
		this.gid = gid;
	}
	public BigDecimal getDistance() {
		return distance;
	}
	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}
	@Override
	public String toString() {
		return "ServiceArea [name=" + name + ", label=" + label + ", lon="
				+ lon + ", lat=" + lat + ", distance=" + distance
				+ ", serviceAreaRadius=" + serviceAreaRadius + "]";
	}
	@Override
	public int compareTo(ServiceArea area2) {
		if (name==null || area2==null){
			return 0;
		}
		if (distance!=null && area2.getDistance()!=null){
			return distance.compareTo(area2.getDistance());
			
		}
		return name.compareTo(area2.name);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((distance == null) ? 0 : distance.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceArea other = (ServiceArea) obj;
		if (distance == null) {
			if (other.distance != null)
				return false;
		} else if (!distance.equals(other.distance))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
}
