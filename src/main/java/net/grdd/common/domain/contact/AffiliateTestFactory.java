/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.HashSet;
import java.util.Set;

import net.grdd.common.domain.Address;

/**
 * @author rezashahbazi
 * @since Oct 31, 2011
 */
public class AffiliateTestFactory {
	// basic reservation (booking only)
	public static Operator constructGenericOperator() {
		Operator operator = new Operator();

		operator.setGid(683L);
		operator.setGriddID("diva");
		operator.setName("Diva Limosuine Ltd");

		Address address = new Address();
		address.setAddress1("1818 Limoland Ave.");
		address.setCity("Los Angeles");
		address.setState("CA");
		address.setZipCode("90025");
		address.setCountry("USA");
		
		operator.setPrimaryAddress(address);
		operator.setWebsite("www.divalimo.com");
		operator.setPhoneNumber("1-800-427-DIVA");
		operator.setFaxNumber("818-762-0197");
		
		Profile profile = new Profile();
		profile.setLogoUrl("http://dev.ggds.com/platform/images/pic_1.jpg");
		profile.setIconUrl("http://dev.ggds.com/platform/images/pic_2.jpg");
		profile.setEmailGeneral("limo@genericlimo.com");
		profile.setMissionStatement("Every customer experiences the dedication and determination of excellence that Diva Limousine brings to chauffeured ground transportation. A combination of the most advanced industry technologies and an extraordinarily keen interest in the people we drive, is the unmistakable Diva hallmark.");
		
		profile.setPromotionTitle("Free limo ride!");
		profile.setPromotionDeal("Buy one get one free in September");
		profile.setPromotionDetails("Book a limo ride for more than $50 and get another limo ride on us! This deal is only available in the month of September so get it while it lasts!");
		profile.setPromotionImageUrl("http://www.gamesourcecolorado.com/buy-1-get-1-free-sale-with-blank-YELLOW-2.jpg");
		
		operator.setProfile(profile);

		
		operator.addConfiguration("adapter.active", "true");

		operator.setServiceAreas(new HashSet<OperatorServiceArea>());

		return operator;
	}

	public static Set<Operator> constructOperators() {
		Set<Operator> operators = new HashSet<Operator>();
		Operator operator1 = new Operator();
		Operator operator2 = new Operator();

		operator1.setGid(1L);
		operator2.setGid(2L);
		operator1.setGriddID("diva");
		operator2.setGriddID("fast");
		operator1.setName("Diva Limosuine");
		operator2.setName("Fast Limosuine");

		Address address1 = new Address();
		address1.setAddress1("11132 Ventura Blvd.");
		address1.setAddress2("Suite 100");
		address1.setCity("Studio City");
		address1.setState("CA");
		address1.setZipCode("91604");
		address1.setCountry("USA");
		
		operator1.setPrimaryAddress(address1);
		operator1.setPhoneNumber("818-555-8292");
		operator1.setFaxNumber("818-555-8265");
		
		Profile profile1 = new Profile();
		profile1.setLogoUrl("http://mediacomtm.com/images/Diva-Limosine-logo.gif");
		profile1.setEmailGeneral("diva@divalimo.com");
		profile1.setMissionStatement("Quality Focused. People Driven.");
		profile1.setAboutUs("Diva Limo");
		profile1.setPromotionImageUrl("http://www.gamesourcecolorado.com/buy-1-get-1-free-sale-with-blank-YELLOW-2.jpg");
		operator1.setProfile(profile1);

		Address address2 = new Address();
		address2.setAddress1("1000 Speedway Ave.");
		address2.setCity("Las Vegas");
		address2.setState("NV");
		address2.setZipCode("89109");
		address2.setCountry("USA");
		
		operator2.setPrimaryAddress(address2);
		operator2.setPhoneNumber("702-555-1231");
		operator2.setFaxNumber("702-555-1414");
		
		Profile profile2 = new Profile();
		profile2.setLogoUrl("http://www.speedylimos.com/wp-content/uploads/2011/05/logo4.jpg");
		profile2.setEmailGeneral("service@speedylimo.com");
		profile2.setMissionStatement("We are speedy.");
		profile2.setAboutUs("Speedy Limo");
		profile2.setPromotionImageUrl("http://appstorehq-production.s3.amazonaws.com/speedboost-iphone-155090.185x185.1267099604.88358.jpg");
		operator2.setProfile(profile2);

		
		
		operator1.addConfiguration("adapter.active", "true");
		operator2.addConfiguration("adapter.active", "true");

		Affiliation affiliation = new Affiliation();
		affiliation.setRequester(operator1);
		affiliation.setRecipient(operator2);
		affiliation.setStatus(AffiliationStatus.ACTIVE);
		Set<Affiliation> affiliationList = new HashSet<Affiliation>();
		affiliationList.add(affiliation);
	/*	operator1.setAffiliations(affiliationList);
		operator1.addAffiliate(operator2);
		operator2.setAffiliations(affiliationList);
		operator2.addAffiliate(operator1);
*/
		operators.add(operator1);
		operators.add(operator2);
		return operators;
	}
}
