/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * Represent Affiliate
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name = "operator_affiliation")
public class Affiliation  extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	private AffiliationStatus status=AffiliationStatus.PENDING;// active, pending, dispab;
	
   // @Type(type="org.joda.time.contrib.hibernate.PersistentDate")
	Date startDate;
	
	@ManyToOne
	@JoinColumn(name="requester_id")
	Company requester;
	
	@JoinColumn(name="requester_contract")
	AffiliationContract requesterContract;
	
	@ManyToOne
	@JoinColumn(name="recipient_id")
	Company recipient; 
	
	@OneToOne
	@JoinColumn(name="recipient_contract")
	AffiliationContract recipientContract;
	
	/*Accept a reservation within giving time in minutes, usually 24 hours before delivery*/
	Integer autoAcceptTime;
	
	/* Time the requester can do the updates */
	Integer updateTimeWindow;
	/* Dates the will not accept the reservations.*/
	String blockoutDates;
	
	public AffiliationStatus getStatus() {
		return status;
	}
	public void setStatus(AffiliationStatus status) {
		this.status = status;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	@JsonIgnore
	@XmlTransient
	public Company getRequester() {
		return requester;
	}
	public void setRequester(Company requester) {
		this.requester = requester;
	}
	
	public AffiliationContract getRequesterContract() {
		return requesterContract;
	}
	public void setRequesterContract(AffiliationContract requesterContract) {
		this.requesterContract = requesterContract;
	}
	@JsonIgnore
	@XmlTransient
	public Company getRecipient() {
		return recipient;
	}
	public void setRecipient(Company recipient) {
		this.recipient = recipient;
	}
	public AffiliationContract getRecipientContract() {
		return recipientContract;
	}
	public void setRecipientContract(AffiliationContract recipientContract) {
		this.recipientContract = recipientContract;
	}
	public Integer getAutoAcceptTime() {
		return autoAcceptTime;
	}
	public void setAutoAcceptTime(Integer autoAcceptTime) {
		this.autoAcceptTime = autoAcceptTime;
	}
	
	public Integer getUpdateTimeWindow() {
		return updateTimeWindow;
	}
	public void setUpdateTimeWindow(Integer updateTimeWindow) {
		this.updateTimeWindow = updateTimeWindow;
	}
	
	public String getBlockoutDates() {
		return blockoutDates;
	}
	public void setBlockoutDates(String blockoutDates) {
		this.blockoutDates = blockoutDates;
	}
	@Override
	public String toString() {
		return "Affiliation [status=" + status + ", startDate=" + startDate
				+ ", requester=" + requester + ", requesterContract="
				+ requesterContract + ", recipient=" + recipient
				+ ", recipientContract=" + recipientContract
				+ ", autoAcceptTime=" + autoAcceptTime + "]";
	}
	
	
	
	
	
 }
