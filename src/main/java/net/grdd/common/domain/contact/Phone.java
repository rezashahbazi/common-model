/*
 * Copyright (c) 2011-2012 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

import net.grdd.common.domain.BaseModel;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author rezashahbazi
 * @since May 27, 2012
 */
@Entity
@Table(name="contact_phone")
public class Phone extends BaseModel {
	

	private static final long serialVersionUID = 1444136553506747304L;
	String number;
	String type;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name="contact_attributes",joinColumns=@JoinColumn(name="attribute_id"))
	@MapKeyColumn(name="attribute_key")
	Map<String, String> attributes ;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public Phone() {
		
	}
	public Phone(String number, String type) {		
		this.number = number;
		this.type = type;		
	}
	
	public Map<String, String> getAttributes() {
		return attributes;
	}
	public String getAttribute(String key) {
		if (attributes==null) return null;
		return attributes.get(key);
	}
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	public void addAttribute(String key, String value) {
		if (this.attributes ==null){
			this.attributes = new HashMap<String,String>();
		}
		attributes.put(key, value);
	}
	public String toString() {
		return ReflectionToStringBuilder.toString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((attributes == null) ? 0 : attributes.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		if (attributes == null) {
			if (other.attributes != null)
				return false;
		} else if (!attributes.equals(other.attributes))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
}
