/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import net.grdd.common.domain.rateMatrix.RateMatrix;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Custom Reservation Validator
 * 
 * @author Reza Shahbazi
 * 
 */
public class ContactValidator extends LocalValidatorFactoryBean {
	protected final Log logger = LogFactory.getLog(getClass());
	@Override
	public void validate(Object target, Errors errors) {
		validate(target,errors,null);
	}
	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {
		
		super.validate(target, errors);
		
		// ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
		if(target instanceof RateMatrix){
			RateMatrix rateMatrix = (RateMatrix) target;
			
			if (!StringUtils.hasText( rateMatrix.getOperatorId())) {
				errors.rejectValue("griddID", "Gridd ID is missing");
			}
		}else{
			Company company = (Company) target;
			if (!StringUtils.hasText( company.getGriddID())) {
				errors.rejectValue("griddID", "Gridd ID is missing");
			}
		}
		
		/*
		if (!StringUtils.hasText(company.getContactType())){
			company.getContactType();
		}*/
		
	}
	

}
