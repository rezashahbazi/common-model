/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * Holds membership information about the operator including start date, status (active/none-active), etc
 * License info to GGDS
 * @author rezashahbazi
 * @since Oct 13, 2011
 */
@Entity
@Table(name="operator_membership")
public class PlatformMembership extends BaseModel{
	private Date memberSince;

	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getMemberSince() {
		return memberSince;
	}

	public void setMemberSince(Date memberSince) {
		this.memberSince = memberSince;
	}
	
	
}
