/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.contact.chauffeur.Chauffeur;
import net.grdd.common.domain.vehicle.Vehicle;
import net.grdd.common.domain.vehicle.VehicleType;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Main business / operator in the Platform
 * 
 * @author rezashahbazi
 * @since Oct 13, 2011
 */
@Entity
@Table
public class Operator extends Company {

	private static final long serialVersionUID = 3188805293398622610L;
	
	@JsonIgnore
	@XmlTransient
	@Transient
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "svc_operator_id")
	Set<OperatorServiceArea> serviceAreas;

	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_operator_id")
	Set<Vehicle> fleet;

	//@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	//@JoinColumn(name = "ch_operator_id")
	@Transient
	Set<Chauffeur> chauffeurs;

	// subsidiary companies 
	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "sub_operator_id")
	Set<Company> companies;

	
	
	
	// license and membership info

	@OneToOne
	@JoinColumn(name = "membership_id_pk")
	private PlatformMembership membership;
	private Integer percentageOfFleetShown;
	private Boolean isFleetShown;
		
	private OperatorType operatorType =OperatorType.CO;
	
	/*
	 * Company has full license object, but for simplicity use license # for operator 
	 */
	private String licenseNumber;
	
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public Set<OperatorServiceArea> getServiceAreas() {
		return serviceAreas;
	}
	public Set<String> getActiveServiceAreas() {
		if ( serviceAreas!=null){
			Set<String> set = new HashSet<String>();
			for (OperatorServiceArea area : serviceAreas) {
				set.add(area.getLocation().getName());
			}
			return set;
		}
		return null;
	}
	public void setActiveServiceAreas(Set<String> active) {
		
	}
	public void setServiceAreas(Set<OperatorServiceArea> serviceAreas) {
		this.serviceAreas = serviceAreas;
	}

	public OperatorServiceArea addServiceArea(ServiceArea serviceArea) {
		if (this.serviceAreas == null) {
			this.serviceAreas = new HashSet<OperatorServiceArea>();
		}
		if (serviceArea != null) {
			OperatorServiceArea area = new OperatorServiceArea();
			area.setLocation(serviceArea);
			this.serviceAreas.add(area);
			return area;
		}
		return null;
	}

	public Set<Vehicle> getFleet() {
		return fleet;
	}

	public void setFleet(Set<Vehicle> fleet) {
		this.fleet = fleet;
	}

	public Integer getPercentageOfFleetShown() {
		return percentageOfFleetShown;
	}

	public void setPercentageOfFleetShown(Integer percentageOfFleetShown) {
		this.percentageOfFleetShown = percentageOfFleetShown;
	}

	public Boolean getIsFleetShown() {
		return isFleetShown;
	}

	public void setIsFleetShown(Boolean isFleetShown) {
		this.isFleetShown = isFleetShown;
	}

	public Set<Chauffeur> getChauffeurs() {
		return chauffeurs;
	}

	public void setChauffeurs(Set<Chauffeur> chauffeurs) {
		this.chauffeurs = chauffeurs;
	}

	public Set<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

	

	public PlatformMembership getMembership() {
		return membership;
	}

	public void setMembership(PlatformMembership membership) {
		this.membership = membership;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public List<VehicleType> getMappedVehicleTypes(){
		String map = super.getConfiguration("vehicle_types");
		List<VehicleType> list = null;
		if (StringUtils.isNotBlank(map)){			
			try {
				VehicleType[] myObjects = mapper.readValue(map, VehicleType[].class);	
				list = Arrays.asList(myObjects);
			} catch (Exception e) {				
				e.printStackTrace();
			}
		}
		return list;	
	}
	@Transient
	public List<String> getVehicleTypes(){
		String map = super.getConfiguration("vehicle_types");
		List<String> list = new ArrayList<String>();
		if (StringUtils.isNotBlank(map)){			
			try {
				VehicleType[] myObjects = mapper.readValue(map, VehicleType[].class);
				for (int i = 0; i < myObjects.length; i++) {
					VehicleType vehicleType = myObjects[i];
					if (StringUtils.isNotBlank(vehicleType.getValue())){
						list.add(vehicleType.getType());
					}
				}
				
			} catch (Exception e) {				
				e.printStackTrace();
			}
		}
		return list;
		
		
	}
	
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public Map<String,String> getMappedVehicleTypesAsMap(){
		List<VehicleType> list = getMappedVehicleTypes();
		Map<String,String>  map = null;
		if (list!=null){
			map = new HashMap<String, String>();
			for (VehicleType vehicleType : list) {
				map.put(vehicleType.getType(), vehicleType.getValue());
			}
		}
		return map;
	}
	
	@Override
	public String toString() {
		return "Operator [operatorId=" + getGriddID() + ", getName()="
				+ getName() + ", getPrimaryAddress()=" + getPrimaryAddress()
				+ ", getPhoneNumber()=" + getPhoneNumber() + ", getGid()="
				+ getGid() + "]";
	}

	

	public OperatorType getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(OperatorType operatorType) {
		this.operatorType = operatorType;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	

}
