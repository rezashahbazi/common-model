/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeName;

/**
 * Agency is the company which holds (employment or contract) the Agents
 * @author rezashahbazi
 * @since Sep 23, 2011
 */
@JsonTypeName("Agency")
@XmlRootElement(name="Agency")
public class Agency extends Company{

	
	private static final long serialVersionUID = -3499389080004060637L;
	private Set<Agent> agents;

	@JsonIgnore
	@XmlTransient
	public Set<Agent> getAgents() {
		return agents;
	}

	public void setAgents(Set<Agent> agents) {
		this.agents = agents;
	}
	public void addAgent(Agent agent) {
		if (this.agents ==null){
			this.agents = new HashSet<Agent>();
		}
		if (agent!=null){
			agent.setAgency(this);
			agents.add(agent);
		}
	}
	
}
