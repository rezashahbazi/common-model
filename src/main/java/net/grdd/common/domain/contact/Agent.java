/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Agent is person works for an Agency
 * @author rezashahbazi
 * @since Sep 23, 2011
 */

@XmlRootElement(name="Agent")
public class Agent extends Person {

	private static final long serialVersionUID = -6617494099883697649L;
	private String agentId;
	
	private Agency agency;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}
	
	
}
