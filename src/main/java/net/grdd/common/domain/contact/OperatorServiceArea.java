/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.contact;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.vehicle.Vehicle;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name="operator_service_area")
public class OperatorServiceArea extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="service_area")
	ServiceArea location;
	
	 @ManyToOne(cascade=CascadeType.MERGE)
	 @JoinColumn(name="svc_operator_id")
	Operator operator;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="service_vehicle")
	Set<Vehicle> vehicles;
	
	public OperatorServiceArea() {
		
	}
	
	public OperatorServiceArea(String serviceArea) {
		this.location = new ServiceArea(serviceArea, 10);
	}
	
	public ServiceArea getLocation() {
		return location;
	}
	public void setLocation(ServiceArea location) {
		this.location = location;
	}
	@JsonIgnore
	@XmlTransient
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	@JsonIgnore
	@XmlTransient
	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	
	
	
	
}
