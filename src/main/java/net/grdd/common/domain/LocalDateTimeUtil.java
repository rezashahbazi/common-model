/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain;

import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * Date conversion
 * @author rezashahbazi
 * @since Sep 22, 2011
 */
public enum LocalDateTimeUtil {
	INSTANCE;
	public String convertTimeZoneFromUTC(   final Date date, final String pattern, final String toTimeZone){
		if (date == null){
			return null;
		}
		final DateTimeFormatter outputFormatter 
        = DateTimeFormat.forPattern(pattern);
		DateTime dt = new LocalDateTime(date).toDateTime(DateTimeZone.UTC);  
		if (StringUtils.isNotBlank(toTimeZone)){
			DateTimeZone forTimeZone = null;
			if (NumberUtils.isNumber(toTimeZone.replace("+", ""))){				
				String[] split = StringUtils.split(toTimeZone,".");
				int hour =  new Integer(split[0]);
				int minutes = split.length>1?new Integer(split[1]):0;
				forTimeZone = DateTimeZone.forOffsetHoursMinutes(hour, minutes);
			}else{
				forTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone(toTimeZone));
			}
			
			DateTime target = dt.withZone(forTimeZone);
			return outputFormatter.print(target);
		}
	    
	    return outputFormatter.print(dt);
	}
}
