package net.grdd.common.domain;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;

@JacksonStdImpl
public class DateDeserializer extends JsonDeserializer<Date> {
//	String[] dateFormats = new String[]{"yyyy-MM-dd'T'HH:mm:ss","yyyy-MM-dd'T'HH:mm:ssZ","yyyy-MM-dd'T'HH:mm:ssXXX","yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSXXX"};
	String[] dateFormats = new String[]{"yyyy-MM-dd'T'HH:mm:ss","yyyy-MM-dd'T'HH:mm:ssZ","yyyy-MM-dd'T'HH:mm:ssXXX"};

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		String date = jp.getText();

		try {			
			return DateUtils.parseDate(date, this.dateFormats);
		} catch (java.text.ParseException e) {
			throw new IOException("Can not parse date format:" + date);
		}
		

	}

}
