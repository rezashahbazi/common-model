/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.vehicle;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.contact.OperatorServiceArea;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name="vehicle")
public class Vehicle extends BaseModel{	
	

	private static final long serialVersionUID = 1407717059935176019L;
	
	
	
	
	private String vehicleId;
	private String vin;
	private Integer passengerCount;

	@ManyToOne
	@JoinColumn(name="vehicle_type_id")
	VehicleType vehicleType;
	private String make;
	private String model;
	private String year;
	private String mileage;
	
	private Boolean active;

	@OneToMany
	@JoinColumn(name="vehicle_id")
	Set<OperatorServiceArea> serviceAreas;
	
	private String color;
	
	private String driverId;
	
	private String email;
	
	private String lastDriverId;
	
	public Vehicle() {
		super();		
	}
	public Vehicle(String vehicleId) {
		super();		
		this.vehicleId = vehicleId;
	}
	public Vehicle(String vehicleId, VehicleType vehicleType) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleType = vehicleType;
	}
	
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public Integer getPassengerCount() {
		return passengerCount;
	}
	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}
	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMileage() {
		return mileage;
	}
	public void setMileage(String mileage) {
		this.mileage = mileage;
	}
	@JsonIgnore
	@XmlTransient
	public Set<OperatorServiceArea> getServiceAreas() {
		return serviceAreas;
	}

	public void setServiceAreas(Set<OperatorServiceArea> serviceAreas) {
		this.serviceAreas = serviceAreas;
	}

	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@Override
	public String toString()
	{
		return "Vehicle [vehicleId=" + vehicleId + ", vin=" + vin +
			", passengerCount=" + passengerCount + ", vehicleType=" +
			vehicleType + ", serviceAreas=" + serviceAreas + ",color ="+color+"]";
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastDriverId() {
		return lastDriverId;
	}
	public void setLastDriverId(String lastDriverId) {
		this.lastDriverId = lastDriverId;
	}

}
