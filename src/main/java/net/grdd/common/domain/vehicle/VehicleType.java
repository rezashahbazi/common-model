/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.vehicle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name="lookup_vehicle_type")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class VehicleType extends BaseModel
{

	private static final long serialVersionUID = 7039919918656033823L;
	// SEDAN, SUV
	@Column(unique=true)
	private String type;
	
	private String value;

	public VehicleType(String type)
	{
		this.type = type;
	}

	public VehicleType(String type, String value)
	{
		this.type = type;
		this.value = value;
	}
	public VehicleType()
	{
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	@Override
	public String toString()
	{
		return type;
	}
	

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VehicleType other = (VehicleType) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public final String[] getStandardTypes(){
	/*	final  Set<VehicleType> list = new HashSet<VehicleType>();
		list.add(new VehicleType("SEDAN"));
		list.add(new VehicleType("SUV"));
		list.add(new VehicleType("STRETCH"));
		list.add(new VehicleType("VAN"));
		list.add(new VehicleType("COACH"));
		
		return list.toArray(new String[]{});*/
		return new String[] {"SEDAN","SUV","STRETCH","VAN","COACH"};
	}
}
