package net.grdd.common.domain.vehicle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class VehicleAssignResult {
	private boolean status;
	private String message;
	
	public VehicleAssignResult(){
		
	}
	
	public VehicleAssignResult(boolean status, String message){
		super();
		this.status = status;
		this.message = message;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof VehicleAssignResult) {
			VehicleAssignResult other = (VehicleAssignResult)obj;
			if(this.status == other.status) {
				if(this.message == null && other.message == null) {
					return true;
				} else if(this.message.equals(other.message)) {
					return true;
				}
			}
		}
		return false;
	}
}
