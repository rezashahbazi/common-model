package net.grdd.common.domain.vehicle;

import java.util.Date;

public class Assignment {

	private String griddId;
	private String vehicleId;
	private String chauffeurId;

	private String driverName;
	private Date timestamp;

	public String getGriddId() {
		return griddId;
	}

	public void setGriddId(String griddId) {
		this.griddId = griddId;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getChauffeurId() {
		return chauffeurId;
	}

	public void setChauffeurId(String chauffeurId) {
		this.chauffeurId = chauffeurId;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}
