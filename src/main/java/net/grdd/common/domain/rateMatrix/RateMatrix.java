/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.rateMatrix;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import net.grdd.common.domain.contact.Company;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * @author edwardhuang
 * @Since Oct 8, 2014 RateMatrix is used to save the basic rate matrix data.
 */
@Entity
@Table(name = "rate_matrix")
@JsonSerialize(include = Inclusion.NON_NULL)
public class RateMatrix {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long gid;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	// make lazy may improve the performance
	@JoinColumn(name = "griddId")
	@JsonIgnore
	private Company company;

	@Transient
	private String operatorId;

	@Transient
	@JsonIgnore
	private LabelType labelType;

	@NotNull
	// @ManyToOne
	@JoinColumn(name = "vehicleId")
	// private VehicleType vehicleType;
	private String vehicleType;

	@NotNull
	@JoinColumn(name = "minFare")
	private double minimumfare;

	@NotNull
	@JoinColumn(name = "minuteRate")
	private double ratePerMinute;

	@NotNull
	@JoinColumn(name = "mileRate")
	private double ratePerMile;

	@NotNull
	@JoinColumn(name = "baseFare")
	private double baseFare;

	@JoinColumn(name = "cancelFee")
	private double cancelFee;

	@JoinColumn(name = "stopFee")
	private double stopFee;

	@JoinColumn(name = "timeStart")
	@JsonIgnore
	private Date busyHourFrom;

	@Transient
	private String busyHourStart;

	@JoinColumn(name = "timeEnd")
	@JsonIgnore
	private Date busyHourTo;

	@Transient
	private String busyHourEnd;
	/**
	 * The specific day of the week
	 */
	@JoinColumn(name = "startWeek")
	private int startWeek;

	@JoinColumn(name = "endWeek")
	private int endWeek;

	/**
	 * busy hour or none-busy hour
	 */
	@NotNull
	@JoinColumn(name = "hourType")
	private String hourType;

	@NotNull
	@JoinColumn(name = "rateType")
	private int rateType;

	/**
	 * custom field
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "rateMatrix")
	private List<CustomField> messageList;

	// /**
	// * label value
	// */
	// @OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL,orphanRemoval=true,mappedBy="rateMatrix")
	// private Set<LabelType> labelSet;
	@Transient
	@JsonIgnore
	private String rateTypeLabel;

	@Transient
	private String startDate;
	@Transient
	private String endDate;

	public List<CustomField> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<CustomField> messageList) {
		this.messageList = messageList;
	}

	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public double getMinimumfare() {
		return minimumfare;
	}

	public void setMinimumfare(double minimumfare) {
		this.minimumfare = minimumfare;
	}

	public double getRatePerMinute() {
		return ratePerMinute;
	}

	public void setRatePerMinute(double ratePerMinute) {
		this.ratePerMinute = ratePerMinute;
	}

	public double getRatePerMile() {
		return ratePerMile;
	}

	public void setRatePerMile(double ratePerMile) {
		this.ratePerMile = ratePerMile;
	}

	public double getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}

	public double getCancelFee() {
		return cancelFee;
	}

	public void setCancelFee(double cancelFee) {
		this.cancelFee = cancelFee;
	}

	public Date getBusyHourFrom() {
		return busyHourFrom;
	}

	public void setBusyHourFrom(Date busyHourFrom) {
		this.busyHourFrom = busyHourFrom;
	}

	public Date getBusyHourTo() {
		return busyHourTo;
	}

	public void setBusyHourTo(Date busyHourTo) {
		this.busyHourTo = busyHourTo;
	}

	public String getHourType() {
		return hourType;
	}

	public void setHourType(String hourType) {
		this.hourType = hourType;
	}

	public int getStartWeek() {
		return startWeek;
	}

	public void setStartWeek(int startWeek) {
		this.startWeek = startWeek;
	}

	public int getEndWeek() {
		return endWeek;
	}

	public void setEndWeek(int endWeek) {
		this.endWeek = endWeek;
	}

	public double getStopFee() {
		return stopFee;
	}

	public void setStopFee(double stopFee) {
		this.stopFee = stopFee;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public LabelType getLabelType() {
		return labelType;
	}

	public void setLabelType(LabelType labelType) {
		this.labelType = labelType;
	}

	public String getBusyHourStart() {
		String startTime = null;
		if (getBusyHourFrom() != null) {
			startTime = new SimpleDateFormat("HH:mm").format(getBusyHourFrom());
		} else {
			startTime = this.busyHourStart;
		}
		return startTime;
	}

	public void setBusyHourStart(String busyHourStart) {
		this.busyHourStart = busyHourStart;
	}

	public String getBusyHourEnd() {
		String endTime = null;
		if (getBusyHourTo() != null) {
			endTime = new SimpleDateFormat("HH:mm").format(getBusyHourTo());
		} else {
			endTime = this.busyHourEnd;
		}
		return endTime;
	}

	public void setBusyHourEnd(String busyHourEnd) {
		this.busyHourEnd = busyHourEnd;
	}

	public int getRateType() {
		return rateType;
	}

	public void setRateType(int rateType) {
		this.rateType = rateType;
	}

	public String getStartDate() {
		return this.startDate;
	}

	public String getEndDate() {
		return this.endDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Transient
	@JsonIgnore
	public String getRateTypeLabel() {
		String[] labels = { "N/A", "By Per Minute", "By Per Mile",
				"Per Minute and Mile", "Per Minute and Mile(Pick highest)" };
		this.rateTypeLabel = labels[this.rateType];
		return rateTypeLabel;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
