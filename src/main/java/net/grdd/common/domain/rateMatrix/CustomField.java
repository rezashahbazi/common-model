/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.rateMatrix;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author edwardhuang
 * @Since Oct 8, 2014
 * CustomField is used to save user defined fee. 
 */


@Entity
@Table(name="rate_custom_field")
public class CustomField {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long gid;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name = "rateId")
	@JsonIgnore
	private RateMatrix rateMatrix;

	@NotNull
	@JoinColumn(name = "fieldName")
	private String fieldName;
	
	@NotNull
	@JoinColumn(name = "customFee")
	private double customFee;

	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	public RateMatrix getRateMatrix() {
		return rateMatrix;
	}

	public void setRateMatrix(RateMatrix rateMatrix) {
		this.rateMatrix = rateMatrix;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public double getCustomFee() {
		return customFee;
	}

	public void setCustomFee(double customFee) {
		this.customFee = customFee;
	}
	
}
