package net.grdd.common.domain.rateMatrix;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.grdd.common.domain.contact.Company;

/**
 * The entity is used to save the label which user can define by
 * himself/herself.
 * 
 * @author edwardhuang
 * @Since Oct 10, 2014
 */

@Entity
@Table(name = "rate_label_type")
public class LabelType {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long gid;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "griddId")
	private Company company;

	@NotNull
	@JoinColumn(name = "vehicleType")
	private String vehicleType;

	@JoinColumn(name = "basicLabel")
	private String basicLabel;

	@JoinColumn(name = "rushLabel")
	private String rushLabel;

	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getBasicLabel() {
		return basicLabel;
	}

	public void setBasicLabel(String basicLabel) {
		this.basicLabel = basicLabel;
	}

	public String getRushLabel() {
		return rushLabel;
	}

	public void setRushLabel(String rushLabel) {
		this.rushLabel = rushLabel;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
