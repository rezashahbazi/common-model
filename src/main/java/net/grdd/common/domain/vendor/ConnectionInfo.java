package net.grdd.common.domain.vendor;

import java.util.Map;

import net.grdd.common.domain.contact.Operator;

/**
 * Hold url, api id and key and goes to the header
 * @author Reza
 * Since 
 */
public class ConnectionInfo {

	private String url;
	private String uri;
	// either s or empty (http/https)
	private String method;
	private String id;
	private String key;
	private String systemId;
	
	// properties specific to SedanMagic
	private String host ;
	private String apiKey ;
	private String adapterProviderId ;
	private String adapterProviderAccountId;
	private String adapterProfileId ;

	
	private String vendorPlatform;
	
	private Operator operator;
	
	private Map<String,String> vehicleTypesMap ;
	public ConnectionInfo() {
		
	}
	
	public ConnectionInfo(String url, String id, String key, String systemId) {
		
		this.url = url;
		this.id = id;
		this.key = key;
		this.systemId = systemId;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public Map<String, String> getVehicleTypesMap() {
		return vehicleTypesMap;
	}

	public void setVehicleTypesMap(Map<String, String> vehicleTypesMap) {
		this.vehicleTypesMap = vehicleTypesMap;
	}

	public String getVendorPlatform() {
		return vendorPlatform;
	}

	public void setVendorPlatform(String vendorPlatform) {
		this.vendorPlatform = vendorPlatform;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}
	
	public String getConfiguration(String key) {
		if (operator!=null){
			return operator.getConfiguration(key);
		}
		return null;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getAdapterProviderId() {
		return adapterProviderId;
	}

	public void setAdapterProviderId(String adapterProviderId) {
		this.adapterProviderId = adapterProviderId;
	}

	public String getAdapterProviderAccountId() {
		return adapterProviderAccountId;
	}

	public void setAdapterProviderAccountId(String adapterProviderAccountId) {
		this.adapterProviderAccountId = adapterProviderAccountId;
	}

	public String getAdapterProfileId() {
		return adapterProfileId;
	}

	public void setAdapterProfileId(String adapterProfileId) {
		this.adapterProfileId = adapterProfileId;
	}

}
