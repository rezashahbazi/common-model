/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */

package net.grdd.common.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="notes")
public class Note 
	extends BaseModel
	implements Comparable<Note>
{
	private static final long serialVersionUID = 1L;

	private String context;
	private String message;
	private String status;
	

	public Note() {
		super();
	}
	public Note(String message) {		
		this.message = message;
	}
	public Note(String context, String message) {
		super();
		this.context = context;
		this.message = message;
	}
	public Note(String user,String context, String message, Date updateTime) {
		
		this.context = context;
		this.message = message;
		setUser(user);
		setUpdateTime(updateTime);
	}

	public String getContext()
	{
		return context;
	}

	public void setContext(String context)
	{
		this.context = context;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
	
	

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString()
	{
		return "Note [context=" + context + ", message=" + message + "]";
	}

	@Override
	public int compareTo(Note n)
	{
		return getCreateDate().compareTo(n.getCreateDate());
	}
}
