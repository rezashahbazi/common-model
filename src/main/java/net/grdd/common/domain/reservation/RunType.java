/*
 * Copyright (c) 2011-2012 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

/**
 * Different RunTypes
 * @author rezashahbazi
 * @since 09/14/2012
 */
public enum RunType {
	
	Airport ("Airport"),
	Transport ("Transport"),
	Directed ("As Directed"),
	Hourly ("Hourly");
	
	String title;
	RunType(String title){
		this.title=title;
	};
}
