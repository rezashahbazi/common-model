/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import net.grdd.common.dispatch.LocationStatusEvent;
import net.grdd.common.domain.Address;
import net.grdd.common.domain.account.Account;
import net.grdd.common.domain.account.Payment;
import net.grdd.common.domain.account.PaymentBankType;
import net.grdd.common.domain.account.PaymentCardType;
import net.grdd.common.domain.contact.Agency;
import net.grdd.common.domain.contact.Agent;
import net.grdd.common.domain.contact.Company;
import net.grdd.common.domain.contact.Passenger;
import net.grdd.common.domain.contact.Phone;
import net.grdd.common.domain.contact.chauffeur.Chauffeur;
import net.grdd.common.domain.vehicle.Vehicle;
import net.grdd.common.domain.vehicle.VehicleType;

/**
 * Create a complete Reservation TEST object and it is valid for testing purpose
 * only
 * 
 * @author rezashahbazi
 * @since Sep 17, 2011
 */
public class ReservationTestFactory {
	//basic reservation (booking only)
	public static Reservation constructReservation() {
		Reservation reservation = new Reservation();
		reservation.setResNo("1234");
		reservation.setCreateDate(new Date());
		reservation.setReservationDate(new Date());
		
		// set client and company
		Account account = new Account();
		account.setAccountNumber("123432-12");
		account.setAccountName("Internal Project");
		account.setCallerName("Julie - VP of Transportation");
		account.setCallerNumber("1-310-500-0000");
		account.setCommentsForDispatcher("Send non-smoking vehciles to my passengers");
		account.setCommentsForDriver("My passengers may have Voucher!");
		Company client = new Company();
		client.setName("31th Century");
		//client.addAccount(account);
		account.setCompany(client);
		reservation.setAccount(account);

		// drop off location and time
		LiveryLocation dropOff = new LiveryLocation();
		dropOff.setAddress1("1123 Ventura Blvd");
		dropOff.setPointOnLocation("Bank of Commerce - Gate A");
		dropOff.setCity("Studio City");
		dropOff.setRoomNumber("23A");
		dropOff.setState("CA");
		dropOff.setPhoneNumber("1-800-123-4554");
		dropOff.setSpecialInstructions("Use front door and be aware of dog");
		dropOff.setTime(new Date());
		
		Locations  locations = new Locations();
		locations.setDropOff(dropOff);

		// set pickup time and location
		LiveryLocation pickup = new LiveryLocation();
		pickup.setAddress1("LAX");
		
		pickup.setCity("Los Angeles");
		pickup.setState("CA");
		pickup.setPhoneNumber("1-800-LAX");
		pickup.setTime(new Date());
		
		pickup.setLocationType("Airport");
		FlightInfo flight= new FlightInfo("LAX", "DL","5745");
		
		Address origin = new Address();
		origin.setCity("Dallas");
		origin.setCountry("USA");
		flight.setOrigin(origin );
		
		Address destination = new Address();
		destination.setCity("Toronto");
		destination.setCountry("Canada");
		
		flight.setDestination(destination );
		flight.setDepartureTime(new Date());
		flight.setArrivalTime(new Date());
		pickup.setFlightInfo(flight);
		pickup.setMeetAndGreet(Boolean.TRUE);
		locations.setPickup(pickup);

		reservation.setPassengerCount(4);

		// add mike as passenger
		Passenger mike = new Passenger();
		mike.setFirstName("Mike");
		mike.setLastName("Smith");
		
		mike.setSpecialInstructions("I will be 5 minutes late");
		mike.setCommentsForDriver("Call 1-800 number before pickup!");
		mike.setCommentsForDispatcher("Send Soda or Water ");
		mike.setImageURL("http://www.flickr.com/photos/chocobeans/4543477479/");
		// poor reza, 
		Phone phone = new Phone("310-500-5540", "mobile");
		phone.addAttribute("sendNotification","true");
		mike.addPhone(phone );
		Address address = new Address();
		address.setAddress("123 main street, Los Angeles, CA, 90011");
		address.setCity("Los Angeles");
		address.setZipCode("90011");
		address.setType("Address");
		mike.addPreference("newspaper", "LA Times");
		mike.addPreference("beverages", "non-alcoholic");
		mike.addAddress(address );
		
		reservation.addPassenger(mike);
		
		Passenger pete = new Passenger();
		pete.setFirstName("Pete");
		pete.setLastName("Anderson");
		reservation.addPassenger(pete);

		VehicleType suv = new VehicleType("SUV");
		reservation.setPreferredVehicleType("SUV");

		LiveryLocation stop1 = new LiveryLocation();
		stop1.setCity("Beverly Hills");
		stop1.setActualTime(new Date());
		stop1.setSpecialInstructions("Stop1 Special Instruction");
		Passenger passenger = new Passenger();
		passenger.setFirstName("Hugo");
		passenger.setLastName("Weaving");
	
		stop1.addPassengers(passenger );
		// add segments
		locations.addStop(stop1, 1);
		
		reservation.setLocations(locations);

		reservation.addNote("Someone", "Someplace", "Very large note");
		
		// Add extra Services
		Fee newspaper =  new Fee("LA Times", 5.100f);
		newspaper.setType(Fee.TYPE.EXTRA_SERVICE);
		reservation.addFee(newspaper);
		
		Fee defaultGratuity =  new Fee();
		defaultGratuity.setDescription("Standard Gratuity");
		defaultGratuity.setCode("46");
		defaultGratuity.setPercent(new BigDecimal(15.00f));
		defaultGratuity.setType(Fee.TYPE.EXPENSE);
		reservation.addFee(defaultGratuity);

		Payment cc = new Payment();
		
		cc.setPaymentAmount(new BigDecimal(90.02f));
		PaymentCardType  ms = new PaymentCardType();
		ms.setCardType("MS");
		ms.setExpireDate(new Date().toString());
		ms.setCardNumber("4553");
		ms.setCardNumber("Rich Peter");
		cc.setPaymentType(ms );
		
		
		reservation.addPayment(cc);
		Payment bank = new Payment();
		PaymentBankType bofa = new PaymentBankType();
		bofa.setBankAcctName("Bank of America");
		bofa.setBankAcctNumber("1234 3442 3445");
		bofa.setCheckNumber("224");
		bank.setPaymentType(bofa);
		reservation.addPayment(bank);
		
		reservation.setPreferredChauffeurNo("0909");
		reservation.setPreferredVehicleNo("013");
		
		reservation.setUser("operatoruser");
		
		LocationStatusEvent location = new LocationStatusEvent(-90.389503f,38.762604f);
		
		reservation.setLocation(location);
		return reservation;
	}
	/**
	 * Create Reservation with Farmin/out info
	 */
	public static Reservation constructReservationWithFarmin() {
		Reservation reservation = constructReservation();
		
		reservation.setAccount(null);
		reservation.setStatus(ReservationStatus.BOOKING_CONFIRMED);
		AffiliateReservation af = new AffiliateReservation();
		reservation.setGriddID("Sandbox");
		af.setProviderId("Sandbox");
		
		af.setRequesterId("FoxTV");
		af.setRequesterResNo("1000001");
		
		reservation.setTransactionId(UUID.randomUUID().toString());

		Agency laxTravelAgnecy = new Agency();
		laxTravelAgnecy.setName("Lax Travel Agency");
		laxTravelAgnecy.setTaxId("1023-134");
		Agent agent = new Agent();
		agent.setFirstName("Steve");
		agent.setLastName("Good Seller");
		agent.setAgentId("LAX-2342");
		laxTravelAgnecy.addAgent(agent);
		af.setAgent(agent);
		//af.setReservation(reservation);

		reservation.setAffiliateReservation(af);
		return reservation;
	}
	/**
	 * Reservation with Dispatch info (code1 and  code3)
	 * @return
	 */

	public static Reservation constructReservationWithDispatch() {
		Reservation reservation = constructReservation();
		reservation.setUpdateTime(new Date());
		ReservationDispatch reservationDispatch = new ReservationDispatch();
		Chauffeur chauffeur = new Chauffeur();
		chauffeur.setFirstName("Jeff");
		chauffeur.setLastName("Hanger");
		chauffeur.setChauffeurId("4321");
		reservationDispatch.setChauffeur(chauffeur);
		VehicleType suv = new VehicleType("SUV");
		Vehicle vehicle = new Vehicle("0128", suv);
		reservationDispatch.setVehicle(vehicle);
		reservation.setReservationDispatch(reservationDispatch);

		LiveryLocation code1 = new LiveryLocation();
		code1.setLat(new BigDecimal(34.053833));
		code1.setLon(new BigDecimal(-118.430085));
		code1.setActualTime(new Date());
		code1.setDispatchCode(ReservationStatus.DISPATCH_ACKNOWLEDGED);
		LiveryLocation code3 = new LiveryLocation();
		code3.setLat(new BigDecimal(34.058002));
		code3.setLon(new BigDecimal(-118.423669));
		code3.setActualTime(new Date());
		code3.setDispatchCode(ReservationStatus.DISPATCH_ON_LOCATION);

		reservationDispatch.addLiveryLocation(code1);
		reservationDispatch.addLiveryLocation(code3);
		reservation.setStatus(ReservationStatus.DISPATCH_ASSIGNED);
		//reservationDispatch.setReservation(reservation);
		return reservation;
	}
}
