/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.Address;
import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;
import net.grdd.common.domain.contact.Passenger;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.hibernate.annotations.Index;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * This is either a pickup or drop off location.
 * 
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name="reservation_livery_location")
public class LiveryLocation extends Address implements Comparable<LiveryLocation> {

	private static final long serialVersionUID = -2141355517918907412L;
	
	// location name, e.g Staples Center
	private String name;
	
	// Location (e.g. Door 'A' )
	private String pointOnLocation;
	/**
	 * This is the scheduled time
	 */	
	@Indexed
	@Index(name="time_index")	
	private Date time;
	
	// e.g Actual Pickup Time
	private Date actualTime;
	
	// i.e LAX
	private String landmark;
	
	private String phoneNumber;
	
	@ManyToOne
	@JoinColumn(name="flight_id")
	private FlightInfo flightInfo;
	
	// Airport, Address, Landmark etc.
	@Transient
	@JsonProperty("locationType")
	private String locationType;

	// custom field per client.
	private String zoneCode;

	private Integer stopNumber;

	// true if this stop is a new stop and not present in booked reservation
	private Boolean newStop;
	
	//e.g action 3 (on location)
	private ReservationStatus dispatchCode;
	
	private String specialInstructions;

	private Boolean meetAndGreet;
	
	private String greetingSign;
	
	/*
	 * These are the stops passengers 
	 * if you are a tour guide, you may have list of names, for each stop.
	 */
	@OneToMany(cascade=CascadeType.PERSIST)
	@JoinColumn(name="location_id")
	private Set<Passenger> passengers;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getTime()
	{
		return time;
	}

	public void setTime(Date time)
	{
		this.time = time;
	}

	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getActualTime()
	{
		return actualTime;
	}

	public void setActualTime(Date actualTime)
	{
		this.actualTime = actualTime;
	}

	public String getLandmark()
	{
		return landmark;
	}

	public void setLandmark(String landmark)
	{
		this.landmark = landmark;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	
	public FlightInfo getFlightInfo()
	{
		return flightInfo;
	}

	public void setFlightInfo(FlightInfo flightInfo)
	{
		this.flightInfo = flightInfo;
	}

	public String getLocationType()
	{
		return locationType;
	}

	public void setLocationType(String locationType)
	{
		this.locationType = locationType;
	}

	public String getZoneCode()
	{
		return zoneCode;
	}

	public void setZoneCode(String zoneCode)
	{
		this.zoneCode = zoneCode;
	}

	public Integer getStopNumber()
	{
		return stopNumber;
	}

	public void setStopNumber(Integer stopNumber)
	{
		this.stopNumber = stopNumber;
	}

	
	public Boolean getNewStop() {
		return newStop;
	}

	public void setNewStop(Boolean newStop) {
		this.newStop = newStop;
	}

	public ReservationStatus getDispatchCode() {
		return dispatchCode;
	}

	public void setDispatchCode(ReservationStatus dispatchCode) {
		this.dispatchCode = dispatchCode;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getPointOnLocation() {
		return pointOnLocation;
	}

	public void setPointOnLocation(String pointOnLocation) {
		this.pointOnLocation = pointOnLocation;
	}

	
	public Boolean getMeetAndGreet() {
		return meetAndGreet;
	}

	public void setMeetAndGreet(Boolean meetAndGreet) {
		this.meetAndGreet = meetAndGreet;
	}
			

	public String getGreetingSign() {
		return greetingSign;
	}

	public void setGreetingSign(String greetingSign) {
		this.greetingSign = greetingSign;
	}

	public Set<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(Set<Passenger> passengers) {
		this.passengers = passengers;
	}
	public void addPassengers(Passenger passenger) {
		if (this.passengers ==null){
			passengers= new HashSet<Passenger>();			
		}
		this.passengers.add(passenger);
	}

	@Override
	public String toString() {
		return "LiveryLocation [time=" + time + ", actualTime=" + actualTime
				+ ", landmark=" + landmark + ", phoneNumber=" + phoneNumber
				+ ", flightInfo=" + flightInfo + ", locationType="
				+ locationType + ", zoneCode=" + zoneCode + ", stopNumber="
				+ stopNumber + ", newStop=" + newStop + ", dispatchCode="
				+ dispatchCode + ", specialInstructions=" + specialInstructions + "]";
	}
	public String toAddressString() {
		return  
				 (getAddress1()!=null? " " + getAddress1() :"")
				+ (getAddress2()!=null? " " + getAddress2() :"")
				+ (getAddress3()!=null? " " + getAddress3() :"")
				+ (landmark!=null? ", " + landmark :"")
				+ (getCity()!=null? ", " + getCity() :"")
				+ (getState()!=null? " " + getState() :"")
				+ (getZipCode()!=null? " " + getZipCode() :"");
				
	}
	/**
	 * We should emiliate empty addresses (specially empty stops)
	 * @return
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public boolean isEmpty(){
		if (StringUtils.isNotBlank(landmark)|| StringUtils.isNotBlank(this.getAddress())||
				StringUtils.isNotBlank(this.getAddress1())||
				StringUtils.isNotBlank(this.getCity())||
				StringUtils.isNotBlank(this.getZipCode()))
				{
			return false;
		}
		return true;
	}
	/**
	 * This is either a short address or flight info
	 * @return
	 */
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getShortLocation(){
		StringBuilder address =new StringBuilder();
		address.append(StringUtils.isNotBlank(this.landmark)?this.landmark +" ":"");
		if (StringUtils.isNotBlank( this.getShortAddress())){
			address.append(this.getShortAddress());
		}
		// try flight info
		if (this.flightInfo!=null){
			address.append(this.flightInfo.getShortFlightInfo());
		}
		return address.toString();
		
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((actualTime == null) ? 0 : actualTime.hashCode());
		result = prime * result
				+ ((dispatchCode == null) ? 0 : dispatchCode.hashCode());
		result = prime * result
				+ ((flightInfo == null) ? 0 : flightInfo.hashCode());
		result = prime * result
				+ ((landmark == null) ? 0 : landmark.hashCode());
		result = prime * result
				+ ((locationType == null) ? 0 : locationType.hashCode());
		result = prime * result
				+ ((meetAndGreet == null) ? 0 : meetAndGreet.hashCode());
		result = prime * result + ((newStop == null) ? 0 : newStop.hashCode());
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((pointOnLocation == null) ? 0 : pointOnLocation.hashCode());
		result = prime
				* result
				+ ((specialInstructions == null) ? 0 : specialInstructions
						.hashCode());
		result = prime * result
				+ ((stopNumber == null) ? 0 : stopNumber.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result
				+ ((zoneCode == null) ? 0 : zoneCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LiveryLocation other = (LiveryLocation) obj;
		if (actualTime == null) {
			if (other.actualTime != null)
				return false;
		} else if (!actualTime.equals(other.actualTime))
			return false;
		if (dispatchCode != other.dispatchCode)
			return false;
		if (flightInfo == null) {
			if (other.flightInfo != null)
				return false;
		} else if (!flightInfo.equals(other.flightInfo))
			return false;
		if (landmark == null) {
			if (other.landmark != null)
				return false;
		} else if (!landmark.equals(other.landmark))
			return false;
		if (locationType == null) {
			if (other.locationType != null)
				return false;
		} else if (!locationType.equals(other.locationType))
			return false;
		if (meetAndGreet == null) {
			if (other.meetAndGreet != null)
				return false;
		} else if (!meetAndGreet.equals(other.meetAndGreet))
			return false;
		if (newStop == null) {
			if (other.newStop != null)
				return false;
		} else if (!newStop.equals(other.newStop))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (pointOnLocation == null) {
			if (other.pointOnLocation != null)
				return false;
		} else if (!pointOnLocation.equals(other.pointOnLocation))
			return false;
		if (specialInstructions == null) {
			if (other.specialInstructions != null)
				return false;
		} else if (!specialInstructions.equals(other.specialInstructions))
			return false;
		if (stopNumber == null) {
			if (other.stopNumber != null)
				return false;
		} else if (!stopNumber.equals(other.stopNumber))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (zoneCode == null) {
			if (other.zoneCode != null)
				return false;
		} else if (!zoneCode.equals(other.zoneCode))
			return false;
		return true;
	}
	@Override
	public int compareTo(LiveryLocation o) {
	     return new CompareToBuilder()
	       .appendSuper(super.compareTo((Address)o))
	       .append(this.name, o.name)
	       .append(this.time, o.time)
	       .append(this.flightInfo, o.flightInfo)
	       .toComparison();
	   }
}
