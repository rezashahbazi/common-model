/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.math.BigDecimal;
import java.util.Currency;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Represents the Extra Services defined in a Reservation. Agent, Dispatcher or
 * Driver can add these extra services.
 * <p> Some fee codes from OTA:  Look up at Vehicle Charge Purpose Type	VCP
 * 38	Tolls
 * 45	Gratuity
 * 46	Standard gratuity
 * 47	Extra gratuity
 * 	48	Parking
 * 49	Airport fee
 * 50	Fuel surcharge
 * 51	Meet &	 greet
 * @author rezashahbazi
 * @since Oct 21, 2011
 */
@Entity
@Table(name = "reservation_fee")
public class Fee extends BaseModel
{
	private static final long serialVersionUID = 5536002008958886572L;

	// This enum may need some tweaking...
	public static enum TYPE
	{
		EXTRA_SERVICE,
		EXPENSE
	};

	private TYPE type;
	// Purpose 35 = "Gratuity". See OpenTravel Code List Vehicle Charge Purpose Type (VCP). 
	// 38: Notice that tolls are extra.
	private String code;
	private String description;
	private BigDecimal fee;
	private BigDecimal percent;
	// TODO: We need to think harder. It may go to Reservation object.
	private Currency currency = Currency.getInstance("USD");
	private Boolean reimbursable;

	public Fee()
	{

	}

	public Fee(String description, double fee)
	{
		this.description = description;
		this.fee = new BigDecimal(fee);
	}

	public Fee(String description, BigDecimal fee)
	{
		this.description = description;
		this.fee = fee;
	}
	public Fee(TYPE type,String description, BigDecimal fee)
	{
		this.type = type;
		this.description = description;
		this.fee = fee;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	@Min(0)
	public BigDecimal getFee()
	{
		return fee;
	}

	public void setFee(BigDecimal fee)
	{
		this.fee = fee;
	}

	@Override
	public String toString()
	{
		return "Fee [type=" + type + ", code=" + code + ", description=" +
			description + ", fee=" + fee + ", percent=" + percent +
			", currency=" + currency + ", reimbursable=" + reimbursable + "]";
	}

	public TYPE getType()
	{
		return type;
	}

	public void setType(TYPE type)
	{
		this.type = type;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public Currency getCurrency()
	{
		return currency;
	}

	@Transient
	@XmlTransient
	@JsonIgnore
	public void setCurrency(Currency currency)
	{
		this.currency = currency;
	}


	public Boolean getReimbursable()
	{
		return reimbursable;
	}

	public void setReimbursable(Boolean reimbursable)
	{
		this.reimbursable = reimbursable;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((fee == null) ? 0 : fee.hashCode());
		result = prime * result + ((percent == null) ? 0 : percent.hashCode());
		result = prime * result
				+ ((reimbursable == null) ? 0 : reimbursable.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fee other = (Fee) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (fee == null) {
			if (other.fee != null)
				return false;
		} else if (!fee.equals(other.fee))
			return false;
		if (percent == null) {
			if (other.percent != null)
				return false;
		} else if (!percent.equals(other.percent))
			return false;
		if (reimbursable == null) {
			if (other.reimbursable != null)
				return false;
		} else if (!reimbursable.equals(other.reimbursable))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	
}
