/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.Address;
import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * Information about a flight, including airline, flight number, departure and arrival time
 * Similar to "Airline" and "OperatingAirlineType" and 
 * @author rezashahbazi
 * @since Dec 19, 2011
 */
@Entity
@Table(name="reservation_flight_info")
public class FlightInfo extends BaseModel implements Comparable<FlightInfo>{
	
	@JsonIgnore
	@XmlTransient
	@Transient
	private static final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
	private static final long serialVersionUID = -8666721862806402117L;

	// origin 
	private Address origin;
	
	// target location
	private Address destination;
	
	private String airportCode; //AA
	private String airlineCode; //AA
	private String flightNumber; //23
	
	private Date departureTime;
	
	// estimated
	private Date arrivalTime;
	
	@XmlTransient
	@Transient
	public boolean isBlank(){
		return StringUtils.isBlank(airportCode) && StringUtils.isBlank(airlineCode);
	}
	public FlightInfo(String airportCode ,String airlineCode, String flightNumber) {
		this.airportCode = airportCode;
		this.flightNumber=flightNumber;
		this.airlineCode=airlineCode;
	}
	
	public FlightInfo(String airlineCode, String flightNumber) {
		this.flightNumber=flightNumber;
		this.airlineCode=airlineCode;
	}
	public FlightInfo() {
		
	}
	

	public Address getOrigin() {
		return origin;
	}
	public void setOrigin(Address origin) {
		this.origin = origin;
	}
	public Address getDestination() {
		return destination;
	}
	public void setDestination(Address destination) {
		this.destination = destination;
	}
	
	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getShortFlightInfo(){
		return StringUtils.trimToEmpty(airlineCode) + StringUtils.trimToEmpty(flightNumber) + (arrivalTime!=null?"  ETA: "+sdf.format(arrivalTime):"");			
	}
	@Override
	public String toString() {
		return "FlightInfo [airlineCode=" + airlineCode + ", flightNumber="
				+ flightNumber + ", departureTime=" + departureTime
				+ ", arrivalTime=" + arrivalTime + "]";
	}
	@Override
	public int compareTo(FlightInfo o) {
		 return new CompareToBuilder()
	       .append(this.airlineCode, o.airlineCode)
	       .append(this.flightNumber, o.flightNumber)	       
	       .toComparison();
	}
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((airlineCode == null) ? 0 : airlineCode.hashCode());
		result = prime * result
				+ ((airportCode == null) ? 0 : airportCode.hashCode());
		result = prime * result
				+ ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
		result = prime * result
				+ ((departureTime == null) ? 0 : departureTime.hashCode());
		result = prime * result
				+ ((flightNumber == null) ? 0 : flightNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightInfo other = (FlightInfo) obj;
		if (airlineCode == null) {
			if (other.airlineCode != null)
				return false;
		} else if (!airlineCode.equals(other.airlineCode))
			return false;
		if (airportCode == null) {
			if (other.airportCode != null)
				return false;
		} else if (!airportCode.equals(other.airportCode))
			return false;
		if (arrivalTime == null) {
			if (other.arrivalTime != null)
				return false;
		} else if (!arrivalTime.equals(other.arrivalTime))
			return false;
		if (departureTime == null) {
			if (other.departureTime != null)
				return false;
		} else if (!departureTime.equals(other.departureTime))
			return false;
		if (flightNumber == null) {
			if (other.flightNumber != null)
				return false;
		} else if (!flightNumber.equals(other.flightNumber))
			return false;
		return true;
	}
	

}
