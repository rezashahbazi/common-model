/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */

package net.grdd.common.domain.reservation;


/**
 * 
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
public enum ReservationStatus
	{
	BOOKING_DRAFT("B"),
	BOOKING_REJECT("B"),
	BOOKING_CANCEL("X"),
	BOOKING_REQUEST("B"),
	BOOKING_CONFIRMED("B"),
	
	DISPATCH("D"),
	DISPATCH_UN_ASSIGNED("D"),
	DISPATCH_ASSIGNED("D"),
	DISPATCH_ACKNOWLEDGED("1"), // code 1
	DISPATCH_EN_ROUTE("2"), // code 2
	DISPATCH_ON_LOCATION("3"), // code 3
	DISPATCH_PASSENGER_ON_BOARD("4"), // code4
	DISPATCH_NO_SHOW("NS"),
	DISPATCH_REJECT("RJ"), // Driver Reject
	DISPATCH_STOP("7"), // code 7
	DISPATCH_DROPOFF("5"), // code 5
	DISPATCH_COMPLETED("5"), // code 5 (same as closed)
	
	
	
	ACCOUNT_BILLED("A"),
	ACCOUNTING_CLOSED("A"),
	
	VENDOR("");
	
	String code;
	private  ReservationStatus(String code){
		this.code=code;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
