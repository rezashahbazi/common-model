/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import net.grdd.common.domain.BaseModel;

/**
 * Locations contains pickup, drop off and stops, similar to OTA's model (reservation has the Locations)
 * @author rezashahbazi
 * @since Nov 17, 2011
 */
@Entity
@Table(name="reservation_locations")
public class Locations extends BaseModel {

	private static final long serialVersionUID = 6743804540782497625L;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="pickup_id")
	private LiveryLocation pickup;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="dropoff_id")
	private LiveryLocation dropOff;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="location_id")
	private Set<LiveryLocation> stops;
	
	@Valid
	@NotNull
	public LiveryLocation getPickup() {
		return pickup;
	}

	public void setPickup(LiveryLocation pickup) {
		this.pickup = pickup;
	}

	@Valid
	@NotNull
	public LiveryLocation getDropOff() {
		return dropOff;
	}

	public void setDropOff(LiveryLocation dropOff) {
		this.dropOff = dropOff;
	}

	public Set<LiveryLocation> getStops() {
		return stops;
	}

	public void setStops(Set<LiveryLocation> stops) {
		this.stops = stops;
	}
	
	public void addStop(LiveryLocation stop, Integer stopNumber) {
		if (this.stops==null){
			this.stops = new HashSet<LiveryLocation>();
		}
		stop.setStopNumber(stopNumber);
		this.stops.add( stop);
	}

	@Override
	public String toString()
	{
		return "Locations [pickup=" + pickup + ", dropOff=" + dropOff +
			", stops=" + stops + "]";
	}

}
