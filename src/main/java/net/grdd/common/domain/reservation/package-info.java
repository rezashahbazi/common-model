/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */

//@XmlJavaTypeAdapter(value=LocalDateTimeAdapter.class,type=LocalDateTime.class)

package net.grdd.common.domain.reservation;
/*
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import net.grdd.common.domain.LocalDateTimeAdapter;

import org.joda.time.LocalDateTime;

*/