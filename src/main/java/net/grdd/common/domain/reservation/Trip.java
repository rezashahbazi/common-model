/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.account.Account;

/**
 * Logical Grouping of reservation.
 * Trip consist of segments (legs)
 * @author rezashahbazi
 * @since Sep 15, 2011
 */
@XmlRootElement
public class Trip extends BaseModel {		
	private static final long serialVersionUID = -5650141781093476943L;
	private String tripId;
	private Account account;
	
	private Set<Reservation> segments;

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Set<Reservation> getSegments() {
		return segments;
	}

	public void setSegments(Set<Reservation> segments) {
		this.segments = segments;
	}
	
}
