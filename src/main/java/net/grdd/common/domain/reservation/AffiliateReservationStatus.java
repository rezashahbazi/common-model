/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */

package net.grdd.common.domain.reservation;

/**
 * Affiliate Reservation status reflects the status of a reservation between
 * affiliates in context of farm-in/farm-out This can not be combine with
 * ReservationStatus. For example, AffiliateReservationStatus is Accepted but
 * the reservationStatus in un-assigned
 * 
 * @author rezashahbazi
 * @since March 27,2012
 */
public enum AffiliateReservationStatus {
	/*Lets remove UNCONFIRMED and use SUBMITTED*/
	NEW,UNCONFIRMED, SUBMITTED, CONFIRMED, REJECTED, IN_PROGRESS, RIDE_COMPLETED, FAILED, CLOSED, CANCEL,CANCEL_REJECTED,CANCEL_CONFIRMED;
}
