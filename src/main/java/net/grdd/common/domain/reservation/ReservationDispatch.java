/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.contact.chauffeur.Chauffeur;
import net.grdd.common.domain.vehicle.Vehicle;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Reservation's Dispatch Ride information
 * 
 * This object is to be deprecated, all fields to be moved into Reservation
 * Key point of contention is the Locations/LiveryLocation since it may
 * either need augmentation for dispatch codes (2, 3, 5/6, 7/4 combos)
 * or separate list.
 * 
 * 
 * @author reza shahbazi
 * @since Sep 13, 2011
 */
public class ReservationDispatch
	
{
	private static final long serialVersionUID = 1547812003925075148L;

	// Actual Vehicle handles the reservations
	private Vehicle vehicle;

	// Actual Chauffeur
	private Chauffeur chauffeur;

	private Reservation reservation;

	// all dispatch information and action codes 1,2,3 etc 
	// keep in consistent so client's don't need to check for nulls for "none"
	private List<LiveryLocation> liveryLocations = new ArrayList<LiveryLocation>();

	public Vehicle getVehicle()
	{
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle)
	{
		this.vehicle = vehicle;
	}

	public Chauffeur getChauffeur()
	{
		return chauffeur;
	}

	public void setChauffeur(Chauffeur chauffeur)
	{
		this.chauffeur = chauffeur;
	}

	@JsonIgnore
	@XmlTransient
	//When we send the reservationDispatch object to Dispatch Server, we need to hold the reservation object.
	// But have to make sure the reservation.reservationDispatch is null to prevent the circular effect in xml or json 
	public Reservation getReservation()
	{
		return reservation;
	}

	public void setReservation(Reservation reservation)
	{
		this.reservation = reservation;
	}

	public List<LiveryLocation> getLiveryLocations()
	{
		return liveryLocations; // should be immutable?
	}

	public void setLiveryLocations(List<LiveryLocation> liveryLocations)
	{
		this.liveryLocations = liveryLocations;
	}

	public void addLiveryLocation(LiveryLocation liveryLocation)
	{
		this.liveryLocations.add(liveryLocation);
	}

	@Override
	public String toString()
	{
		return "ReservationDispatch [vehicle=" + vehicle + ", chauffeur=" +
			chauffeur + ", reservation=" + reservation + ", liveryLocations=" +
			liveryLocations + "]";
	}
}
