/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.dispatch.LocationStatusEvent;
import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;
import net.grdd.common.domain.DistanceUnit;
import net.grdd.common.domain.Note;
import net.grdd.common.domain.account.Account;
import net.grdd.common.domain.account.Payment;
import net.grdd.common.domain.contact.Company;
import net.grdd.common.domain.contact.Passenger;
import net.grdd.common.domain.contact.Phone;
import net.grdd.common.domain.reservation.AffiliateReservation.SubmissionTarget;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bson.types.ObjectId;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.hibernate.annotations.Index;
import org.springframework.data.mongodb.core.index.Indexed;


/**
 * Represent a Reservation object
 * db.getCollection('reservation-feedback').ensureIndex({'transactionId':1})
 * @author reza shahbazi
 * @since 10/12/2011
 */
@XmlRootElement(name="Reservation")
//@Entity
//@Table(name="reservation", uniqueConstraints = {@UniqueConstraint(columnNames={"resNo", "operatorId"})})
public class Reservation extends BaseModel {
	
	private static final long serialVersionUID = 721330344849665057L;

	@org.springframework.data.annotation.Id
	@Column(name="_id")
	@JsonIgnore
	@XmlTransient
	private ObjectId id;
	
	@NotNull
	@Index(name="res_index")
	@Indexed
	private String resNo;

	// schema version
	@Transient
	private String schemaVer;

	// Airport
	private String runType;

	//@Type(type="org.joda.time.contrib.hibernate.PersistentDate")
	// Booking Date
	private Date reservationDate ;

	@Transient
	private Date completedDate;

	@Indexed
	private ReservationStatus status;
	
	private Integer passengerCount;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="locations_id")
	private Locations locations;
	
	/*
	 * Stops also have the Set of the passengers.
	 * The first passenger is the primary passenger, this why we used linkedlist
	 * The Reservation's passengers should ideally aggregate stops passengers.
	 */
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="res_no")
	private List<Passenger> passengers;
	
	// Changed from VehicleType to String b/c ggds platform should be able to save raw reservation and does the validation later on 
	private String preferredVehicleType;
	
	// if vehicle type is not one of the standard vehicle type. i.e. LUX-SUV
	private String customVehicleType;
	
	/*  Preferred Vehicle Id and Chauffeur Id*/
	private String preferredVehicleNo;
	
	private String preferredChauffeurNo;
	
	/*  Actual Vehicle Id and Chauffeur Id*/
	/* No longer using ReservationDispatch object, the following two fields will hold the actual chauffeur and vehicle information for a dispatched reservation */
	private String actualVehicleNo;
	
	private String actualChauffeurNo;
	
	private String actualChauffeurName;
	
	@Min(0)
	private BigDecimal baseAmount;
	

	// ETA
	private String estimatedTimeArrival;
	
	// Time is going to take to finish the trip
	private Integer estimatedTime;
	
	@Min(0)
	private BigDecimal estimatedAmount;
	
	@Min(0)
	private BigDecimal totalAmount;
	
	private String specialInstructions;
	
	@OneToMany(cascade=CascadeType.ALL)
	
    @JoinTable(name="reservation_note", 
          joinColumns=@JoinColumn(name="res_no"),
          inverseJoinColumns=@JoinColumn(name="note_id"))
	private List<Note> notes;
	
	// Reference back to Affiliate Reservation (for local reservation is will null)
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="res_affiliate_id")
	private AffiliateReservation affiliateReservation;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="res_no")
	private Set<Fee> fees = new LinkedHashSet<Fee>();
	
	@Transient
	private Fee[] feesArray;
	
	public Fee[] getFeesArray() {
		return feesArray;
	}

	public void setFeesArray(Fee[] feesArray) {
		this.feesArray = feesArray;
	}

	// Collection of Ride or Dispatch Information.
	@Transient
	private ReservationDispatch reservationDispatch;
	
	@Transient
	private Account account;
	
	@Transient
	private Set<Payment> payments;

	// distance in whatever units (miles or km)
	private Double totalTripDist;
	private DistanceUnit distUnit;

	// in minutes? do we need this since it's derived from the liverylocations
	private Integer totalTripDuration;

	// in minutes
	private Integer additionalTime;
	
	@Indexed
	private String transactionId;
	
	//@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	private Date transactionDate;
	
	// POLL, MiniRes, Adapter, LA, etc.
	// Platform Core sets this value by calling affiliate service and check's if the client is using what product
	private transient String targetPlatform = "MINIRES";	
		
	// e.g MiniRes, MyRide, etc
	private String origination;
	
	private String reference;
	
	// Fasttrak, LimoAnywhere,etc
	private String sourceVendor ;
	
	private LocationStatusEvent location;
	
	private Map<String,String> metaData;
	
	private Map<String,String> vendor;
	
	//Either normal (default, onDemand, quote
	private String reservationType ;
		
	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getResNo() {
		return resNo;
	}

	public void setResNo(String resNo) {
		this.resNo = resNo;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public String getResKey() {
		return getGriddID()+":"+resNo;
	}
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getProviderResKey() {
		if (this.affiliateReservation!=null){
			return affiliateReservation.getProviderId()+":"+affiliateReservation.getProviderResNo();
		}
		return null;
	}
	public String getRunType() {
		return runType;
	}

	public void setRunType(String runType) {
		this.runType = runType;
	}
	
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public ReservationStatus getStatus() {
		return status;
	}
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getFullStatus() {
		if (status == null){
			return null;
		}
		if (affiliateReservation==null){
			return status.name();
		}
		
		return status.name()+(this.affiliateReservation.getStatus()!=null? ":"+this.affiliateReservation.getStatus().name():"");
	}

	
	public Locations getLocations() {
		return locations;
	}

	public void setLocations(Locations locations) {
		this.locations = locations;
	}

	public void setStatus(ReservationStatus status) {
		this.status = status;
	}

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

	
	public List<Passenger> getPassengers() {
		return passengers;
	}
	/**
	 * The first passenger is the primary passenger
	 * @return
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public Passenger getPrimaryPassenger() {
		if ( passengers !=null && !passengers.isEmpty()){
			return passengers.iterator().next();
		}
		return null;	
	}
	/**
	 * If we want to send the notification to the passenger
	 * @return
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getPrimaryPassengerMobile() {
		Passenger passenger = getPrimaryPassenger();
		if (passenger!=null ){			
			Phone phone = passenger.getPhoneByType("Mobile");
			if (phone!=null && "true".equalsIgnoreCase( phone.getAttributes().get("sendNotification"))){
				return phone.getNumber();
			}
		}
		return null;	
	}
	/**
	 * If we want to send the notification to the passenger's email
	 * @return
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getPrimaryPassengerEmail() {
		Passenger passenger = getPrimaryPassenger();
		if (passenger!=null ){
			return passenger.getEmail();			
		}
		return null;	
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}
	
	public void addPassenger(Passenger passenger) {
		if (this.passengers ==null){
			this.passengers =new LinkedList<Passenger>();
		}			
		passengers.add(passenger);
	}
	
	public String getPreferredVehicleType() {
		return preferredVehicleType;
	}

	public void setPreferredVehicleType(String preferredVehicleType) {
		this.preferredVehicleType = preferredVehicleType;
	}	

	public String getCustomVehicleType() {
		return customVehicleType;
	}

	public void setCustomVehicleType(String customVehicleType) {
		this.customVehicleType = customVehicleType;
	}

	public String getPreferredVehicleNo() {
		return preferredVehicleNo;
	}

	public void setPreferredVehicleNo(String preferredVehicleNo) {
		this.preferredVehicleNo = preferredVehicleNo;
	}

	public String getPreferredChauffeurNo() {
		return preferredChauffeurNo;
	}

	public void setPreferredChauffeurNo(String preferredChauffeurNo) {
		this.preferredChauffeurNo = preferredChauffeurNo;
	}

	@Min(0)
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	
	public BigDecimal getEstimatedAmount() {
		return estimatedAmount;
	}

	public void setEstimatedAmount(BigDecimal estimatedAmount) {
		this.estimatedAmount = estimatedAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	
	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public List<Note> getNotes() {
		return this.notes;
	}
	
	public boolean hasError() {
		if ( notes == null) return false;
		for (Note note : this.notes) {
			if ("error".equalsIgnoreCase(note.getStatus()) || (note.getMessage()!=null &&  note.getMessage().indexOf("Error") == 0)){
				return true;
			}
			
		}
		return false;
	}
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getNotesString() {
		if (notes==null){
			return null;
		}
		StringBuffer logs = new StringBuffer();
		for (Note note : notes) {
			if (logs.length()!=0){
				logs.append(" ,");
			}
			logs.append(note.getMessage());			
		}
		return logs.toString();
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public void addNote(String user, String context, String message)
	{
		Note n = new Note();
		n.setUser(user);
		n.setContext(context);
		n.setMessage(message);

		if (null == this.notes)
			this.notes = new ArrayList<Note>();

		this.notes.add(n);
	}
	public void addNote(Note note)
	{
		
		if (null == this.notes)
			this.notes = new ArrayList<Note>();

		this.notes.add(note);
	}
	@JsonIgnore
	//@XmlTransient
	@Transient
	public boolean hasNote(Note note)
	{
		if (this.notes == null || note == null){
			return false;
		}
		
		for (Note in : this.notes) {
			// we may check the context
			if (in.getMessage().equalsIgnoreCase(note.getMessage())){
				return true;
			}
		}
		return false;
	}

	@Valid		
	public AffiliateReservation getAffiliateReservation() {
		return affiliateReservation;
	}

	public void setAffiliateReservation(AffiliateReservation affiliateReservation) {
		this.affiliateReservation = affiliateReservation;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	
	public ReservationDispatch getReservationDispatch() {
		return reservationDispatch;
	}

	public void setReservationDispatch(ReservationDispatch reservationDispatch) {
		this.reservationDispatch = reservationDispatch;
	}

	public Set<Fee> getFees() {
		return fees;
	}
	/**
	 * Comma Delimated Extra Services
	 * @return
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getExtraServices() {
		if (fees==null){
			return "";
		}
		StringBuffer noteStr = new StringBuffer();
		for (Fee note : fees) {
			if (Fee.TYPE.EXTRA_SERVICE.equals( note.getType())){
				if (noteStr.length()>0){
					noteStr.append(",");
				}
				noteStr.append(note.getDescription());
			}
			
		}
		return noteStr.toString();
	}

	public void setFees(Set<Fee> fees) {
		this.fees = fees;
	}

	public void addFee(Fee fee) {
		if (this.fees.isEmpty()){
			this.fees = new LinkedHashSet<Fee>();
		}
		this.fees.add(fee);
	}
	@JsonIgnore
	//@XmlTransient
	@Transient
	public boolean hasFee(Fee fee) {
		if (this.fees ==null || fee == null){
			return false;
		}
		for (Fee in : fees) {
			if (in.equals(fee)){
				return true;
			}
		}
		return false;
	}
	
	public Set<Payment> getPayments() {
		return payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}
	
	public void addPayment(Payment payment) {
		if(this.payments ==null ){
			payments = new HashSet<Payment>();
		}
		payments.add(payment);
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getSourceVendor() {
		return sourceVendor;
	}

	public void setSourceVendor(String sourceVendor) {
		this.sourceVendor = sourceVendor;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE, true);
	}

	public Double getTotalTripDist()
	{
		return totalTripDist;
	}

	public void setTotalTripDist(Double totalTripDist)
	{
		this.totalTripDist = totalTripDist;
	}

	public DistanceUnit getDistUnit()
	{
		return distUnit;
	}

	public void setDistUnit(DistanceUnit distUnit)
	{
		this.distUnit = distUnit;
	}

	public Integer getTotalTripDuration()
	{
		return totalTripDuration;
	}

	public void setTotalTripDuration(Integer totalTripDuration)
	{
		this.totalTripDuration = totalTripDuration;
	}

	public Integer getAdditionalTime()
	{
		return additionalTime;
	}

	public void setAdditionalTime(Integer additionalTime)
	{
		this.additionalTime = additionalTime;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public String getTenantId()
	{
		return getOperatorId() + ":" + this.resNo;
	}
	
	/**
	 * Update full address (XAL format) form street, city, etc 
	 */
	public void updateStandardAddresses(){
		if ( this.getLocations()!=null){
			if (this.getLocations().getPickup()!=null){
				this.getLocations().getPickup().updateStandard();
			}
			if (this.getLocations().getPickup()!=null){
				this.getLocations().getDropOff().updateStandard();
			}
			if (this.getLocations().getStops()!=null){
				for (LiveryLocation stop : this.getLocations().getStops()) {
					stop.updateStandard();
				}
			}
		}
	}
	
	
	
	/**
	 * a reservation can be a local or farmout
	 * @return
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public boolean isLocal(){
		if (this.affiliateReservation==null) {return true;}
		
		// if requester and provider are different then return false;
		if (StringUtils.isBlank( this.getAffiliateReservation().getRequesterId()) && StringUtils.isBlank( this.affiliateReservation.getProviderId())){
			return true;
		}
		
		// if the griddID is the same as provider id
		if (StringUtils.isNotBlank( this.getGriddID()) ){
			if (StringUtils.isNotBlank( this.affiliateReservation.getProviderId()) && ! this.getGriddID().equalsIgnoreCase( this.affiliateReservation.getProviderId())){
				return false;
			}
			if (StringUtils.isNotBlank( this.affiliateReservation.getRequesterId()) && ! this.getGriddID().equalsIgnoreCase( this.affiliateReservation.getRequesterId())){
				return false;
			}
		}
		
		// if requester and provider are different then return false;
		if (StringUtils.isBlank( this.getAffiliateReservation().getRequesterId()) || StringUtils.isBlank( this.affiliateReservation.getProviderId())){
			return true;
		}
		
		if (!this.getAffiliateReservation().getRequesterId().equalsIgnoreCase(this.affiliateReservation.getProviderId())){
			return false;
		}
		
		return true;
		// check if provider and requester are different
		//return  this.affiliateReservation.getProviderId().equalsIgnoreCase( this.affiliateReservation.getRequesterId());
		
		
	}
	@JsonIgnore
	@XmlTransient
	@Transient
	public boolean isProvider(){
		if (this.affiliateReservation==null) {return true;}
		// if the griddID is the same as provider id
		if (StringUtils.isNotBlank( this.getGriddID()) && StringUtils.isNotBlank( this.affiliateReservation.getProviderId()) ){
			return  this.getGriddID().equalsIgnoreCase( this.affiliateReservation.getProviderId());				
		}
		
		return true;
	}
	/**
	 * GGDS platform will use this to get the target operator
	 * Checks if the operatorId is same as RequesterId or Provider Id 
	 * @return GGDS
	 */
	@Transient
	@JsonIgnore
	@XmlTransient
	
	public Company getTargetOperator(){
		if (affiliateReservation!=null){
			// decide based on reservation's griddID and either provider or requester id is matching
			if (affiliateReservation.getRequesterId().equalsIgnoreCase(affiliateReservation.getProviderId())){
				return affiliateReservation.getProvider();
			}
			if (getGriddID()!=null){	
				if( !getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
					return affiliateReservation.getRequester();
				}
				if( !getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
					return affiliateReservation.getProvider();
				}
			}
			return affiliateReservation.getProvider();
		}
		return null;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getTargetAffiliateId(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return affiliateReservation.getRequesterId();
			}
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return affiliateReservation.getProviderId();
			}
			/*
			 * Local Ride (not farmed)
			 */
			if (affiliateReservation.getRequesterId().equalsIgnoreCase(affiliateReservation.getProviderId())){
				return affiliateReservation.getProviderId();
			}
		}
		return null;
	}
	/**
	 * Source provider is the sender of the request
	 * @return
	 */
	@Transient
	@JsonIgnore
	@XmlTransient
	public Company getSourceOperator(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return affiliateReservation.getRequester();
			}
			if( getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return affiliateReservation.getProvider();
			}
			return affiliateReservation.getProvider();
		}
		return null;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getSourceAffiliateId(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return affiliateReservation.getRequesterId();
			}
			if( getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return affiliateReservation.getProviderId();
			}			
		}
		return null;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getTargetResNo(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return affiliateReservation.getRequesterResNo();
			}
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return affiliateReservation.getProviderResNo();
			}			
		}
		return null;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getSourceResNo(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return affiliateReservation.getRequesterResNo();
			}
			if( getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return affiliateReservation.getProviderResNo();
			}			
		}
		return null;
	}
	/*
	@Transient
	@JsonIgnore
	@XmlTransient
	public String getTargetOperatorId(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return affiliateReservation.getRequesterId();
			}
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return affiliateReservation.getProviderId();
			}
		}
		return null;
	}*/
	/**
	 * This will Platform ESB (exchange key).
	 * Bind right exchange from the payload.
	 * either:
	 * ADAPTER -> Send to call url
	 * POLL --> client will poll
	 * DISPATCH --> send to dispatch server, if operator has activiated the CloudDispatch feature.
	 * DEADEND
	 * @return
	 */
	
	
	
	/*public String getTargetDestination(){
		Company   operator = getTargetOperator();
		
		if (this.affiliateReservation!=null && AffiliateReservation.SubmissionTarget.REQUESTER.equals(affiliateReservation.getSubmissionTarget()) ){
			operator = getSourceOperator(); 
		}
		
					
		if (operator !=null ){
			
			if(  StringUtils.isNotBlank(operator.adapterURL() )){
				return "ADAPTER";
			}else{
				return "MINIRES";
			}
			
			
		}
		return "POLL";
	}*/
	// just nothing, amke json deserializer happy 
	//public void setType(ReservationType type){
	@Transient
	@JsonIgnore
	@XmlTransient	
	public String getTargetPlatform() {
		return targetPlatform;
	}

	public void setTargetPlatform(String targetPlatform) {
		this.targetPlatform = targetPlatform;
	}

	//}
	@Transient
	@JsonIgnore
	@XmlTransient
	public ReservationType getType(){
		if (affiliateReservation!=null && getGriddID()!=null){
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getRequesterId())){
				return ReservationType.FARMOUT;
			}
			if( !getGriddID().equalsIgnoreCase( affiliateReservation.getProviderId())){
				return ReservationType.FARMIN;
			}
		}
				
		return ReservationType.LOCAL;
	}
	/**
	 * It is not completed yet, either in dispatch, of booking
	 * @return
	 */
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean isActive(){
		if  ( status.toString().indexOf("DISPATCH")==0  ){
			return true;
		}
		// check the status
		if (affiliateReservation!=null ){
			return (          AffiliateReservationStatus.CONFIRMED.equals(affiliateReservation.getStatus())
							||AffiliateReservationStatus.IN_PROGRESS.equals(affiliateReservation.getStatus())
							||AffiliateReservationStatus.SUBMITTED.equals(affiliateReservation.getStatus())
							||AffiliateReservationStatus.REJECTED.equals(affiliateReservation.getStatus())
							); 
		}
		if (this.status==null) return false;
		
		if  ( ReservationStatus.DISPATCH_UN_ASSIGNED.equals(status) || ReservationStatus.DISPATCH_COMPLETED.equals(status)){
			return false;			
		}
		
		
		
		return false;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean isCancelPhase(){
		if (this.affiliateReservation==null){return false;}
		if (this.affiliateReservation.getStatus()!=null 
				&& affiliateReservation.getStatus().toString().indexOf("CANCEL")>=0 ){
			return true;
		}
		return false;
	}
	/**
	 * Similar to isInDispatch but also checks the affiliateReservation status
	 * @return
	 */
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean isInProgress(){
		if (this.status==null) return false;
		if  ( ReservationStatus.DISPATCH_UN_ASSIGNED.equals(status)){
			return false;			
		}
		if  ( status.toString().indexOf("DISPATCH")==0  || ReservationStatus.BOOKING_REJECT.equals(status)){
			return true;
		}
		if (affiliateReservation!=null && affiliateReservation.getStatus()!=null){
			return (affiliateReservation.getStatus().equals(AffiliateReservationStatus.CONFIRMED)
					||affiliateReservation.getStatus().equals(AffiliateReservationStatus.IN_PROGRESS));
		}
		
		return false;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean isInDispatch(){
		if (this.status==null) return false;
		if  ( ReservationStatus.DISPATCH_UN_ASSIGNED.equals(status)){
			return false;			
		}
		if  ( status.toString().indexOf("DISPATCH")==0 ){
			return true;
		}
				
		return false;
	}
	
	
	
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean isComplete(){
		if (status!=null && ReservationStatus.DISPATCH_COMPLETED.equals(status) || status.toString().indexOf("ACCOUNT")==0){
			return true;
		}
		if (isActive()){
			return false;			
		}
		
		
		return false;
	}
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean isClosed(){
		if (status!=null && ReservationStatus.DISPATCH_COMPLETED.equals(status) && this.completedDate!=null){
			return true;
		}
		
		
		return false;
	}

	public String getActualVehicleNo() {
		return actualVehicleNo;
	}

	public void setActualVehicleNo(String actualVehicleNo) {
		this.actualVehicleNo = actualVehicleNo;
	}

	public String getActualChauffeurNo() {
		return actualChauffeurNo;
	}

	public void setActualChauffeurNo(String actualChauffeurNo) {
		this.actualChauffeurNo = actualChauffeurNo;
	}
	
	public String getOperatorId()
	{
		return getGriddID();
	}
	
	public void setOperatorId(String id)
	{
		setGriddID(id);
	}

	public String getSchemaVer() {
		return schemaVer;
	}

	public void setSchemaVer(String schemaVer) {
		this.schemaVer = schemaVer;
	}
	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)	
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}


	public String getOrigination() {
		return origination;
	}

	public void setOrigination(String origination) {
		this.origination = origination;
	}	
		

	public LocationStatusEvent getLocation() {
		return location;
	}

	public void setLocation(LocationStatusEvent location) {
		this.location = location;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	
	
	public Map<String, String> getMetaData() {
		return metaData;
	}
	public String getMetaData(String key) {
		if (metaData==null){
			return  null;
		}
		return metaData.get(key);
	}

	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}
	public void addMetaData(String key, String  value) {
		if (this.metaData ==null){
			this.metaData =  new HashMap<String, String>();
		}
		metaData.put(key, value);
	}

	public Map<String, String> getVendor() {
		return vendor;
	}
	public String getVendor(String key) {
		if (vendor==null){
			return null;
		}
		return vendor.get(key);
	}

	public void setVendor(Map<String, String> vendor) {
		this.vendor = vendor;
	}

	public void addVendor(String key, String  value) {
		if (this.vendor ==null){
			this.vendor =  new HashMap<String, String>();
		}
		vendor.put(key, value);
	}
	
	
	public String getActualChauffeurName() {
		return actualChauffeurName;
	}

	public void setActualChauffeurName(String actualChauffeurName) {
		this.actualChauffeurName = actualChauffeurName;
	}		
	
	public String getReservationType() {
		return reservationType;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}

	
	public String getEstimatedTimeArrival() {
		return estimatedTimeArrival;
	}

	public void setEstimatedTimeArrival(String estimatedTimeArrival) {
		this.estimatedTimeArrival = estimatedTimeArrival;
	}

	public Integer getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(Integer estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime
				* result
				+ ((actualChauffeurNo == null) ? 0 : actualChauffeurNo
						.hashCode());
		result = prime * result
				+ ((actualVehicleNo == null) ? 0 : actualVehicleNo.hashCode());
		result = prime * result
				+ ((additionalTime == null) ? 0 : additionalTime.hashCode());
		result = prime
				* result
				+ ((affiliateReservation == null) ? 0 : affiliateReservation
						.hashCode());
		result = prime * result
				+ ((baseAmount == null) ? 0 : baseAmount.hashCode());
		result = prime * result
				+ ((completedDate == null) ? 0 : completedDate.hashCode());
		result = prime * result
				+ ((distUnit == null) ? 0 : distUnit.hashCode());
		result = prime * result + ((fees == null) ? 0 : fees.hashCode());
		result = prime * result
				+ ((locations == null) ? 0 : locations.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result
				+ ((passengerCount == null) ? 0 : passengerCount.hashCode());
		result = prime * result
				+ ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result
				+ ((payments == null) ? 0 : payments.hashCode());
		result = prime
				* result
				+ ((preferredChauffeurNo == null) ? 0 : preferredChauffeurNo
						.hashCode());
		result = prime
				* result
				+ ((preferredVehicleNo == null) ? 0 : preferredVehicleNo
						.hashCode());
		result = prime
				* result
				+ ((preferredVehicleType == null) ? 0 : preferredVehicleType
						.hashCode());
		result = prime * result + ((resNo == null) ? 0 : resNo.hashCode());
		result = prime * result
				+ ((reservationDate == null) ? 0 : reservationDate.hashCode());
		result = prime
				* result
				+ ((reservationDispatch == null) ? 0 : reservationDispatch
						.hashCode());
		result = prime * result + ((runType == null) ? 0 : runType.hashCode());
		result = prime * result
				+ ((schemaVer == null) ? 0 : schemaVer.hashCode());
		result = prime
				* result
				+ ((specialInstructions == null) ? 0 : specialInstructions
						.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((totalAmount == null) ? 0 : totalAmount.hashCode());
		result = prime * result
				+ ((totalTripDist == null) ? 0 : totalTripDist.hashCode());
		result = prime
				* result
				+ ((totalTripDuration == null) ? 0 : totalTripDuration
						.hashCode());
		result = prime * result
				+ ((transactionDate == null) ? 0 : transactionDate.hashCode());
		result = prime * result
				+ ((transactionId == null) ? 0 : transactionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (actualChauffeurNo == null) {
			if (other.actualChauffeurNo != null)
				return false;
		} else if (!actualChauffeurNo.equals(other.actualChauffeurNo))
			return false;
		if (actualVehicleNo == null) {
			if (other.actualVehicleNo != null)
				return false;
		} else if (!actualVehicleNo.equals(other.actualVehicleNo))
			return false;
		if (additionalTime == null) {
			if (other.additionalTime != null)
				return false;
		} else if (!additionalTime.equals(other.additionalTime))
			return false;
		if (affiliateReservation == null) {
			if (other.affiliateReservation != null)
				return false;
		} else if (!affiliateReservation.equals(other.affiliateReservation))
			return false;
		if (baseAmount == null) {
			if (other.baseAmount != null)
				return false;
		} else if (!baseAmount.equals(other.baseAmount))
			return false;
		if (completedDate == null) {
			if (other.completedDate != null)
				return false;
		} else if (!completedDate.equals(other.completedDate))
			return false;
		if (distUnit != other.distUnit)
			return false;
		if (fees == null) {
			if (other.fees != null)
				return false;
		} else if (!fees.equals(other.fees))
			return false;
		if (locations == null) {
			if (other.locations != null)
				return false;
		} else if (!locations.equals(other.locations))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (passengerCount == null) {
			if (other.passengerCount != null)
				return false;
		} else if (!passengerCount.equals(other.passengerCount))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (payments == null) {
			if (other.payments != null)
				return false;
		} else if (!payments.equals(other.payments))
			return false;
		if (preferredChauffeurNo == null) {
			if (other.preferredChauffeurNo != null)
				return false;
		} else if (!preferredChauffeurNo.equals(other.preferredChauffeurNo))
			return false;
		if (preferredVehicleNo == null) {
			if (other.preferredVehicleNo != null)
				return false;
		} else if (!preferredVehicleNo.equals(other.preferredVehicleNo))
			return false;
		if (preferredVehicleType == null) {
			if (other.preferredVehicleType != null)
				return false;
		} else if (!preferredVehicleType.equals(other.preferredVehicleType))
			return false;
		if (resNo == null) {
			if (other.resNo != null)
				return false;
		} else if (!resNo.equals(other.resNo))
			return false;
		if (reservationDate == null) {
			if (other.reservationDate != null)
				return false;
		} else if (!reservationDate.equals(other.reservationDate))
			return false;
		if (reservationDispatch == null) {
			if (other.reservationDispatch != null)
				return false;
		} else if (!reservationDispatch.equals(other.reservationDispatch))
			return false;
		if (runType == null) {
			if (other.runType != null)
				return false;
		} else if (!runType.equals(other.runType))
			return false;
		if (schemaVer == null) {
			if (other.schemaVer != null)
				return false;
		} else if (!schemaVer.equals(other.schemaVer))
			return false;
		if (specialInstructions == null) {
			if (other.specialInstructions != null)
				return false;
		} else if (!specialInstructions.equals(other.specialInstructions))
			return false;
		if (status != other.status)
			return false;
		if (totalAmount == null) {
			if (other.totalAmount != null)
				return false;
		} else if (!totalAmount.equals(other.totalAmount))
			return false;
		if (totalTripDist == null) {
			if (other.totalTripDist != null)
				return false;
		} else if (!totalTripDist.equals(other.totalTripDist))
			return false;
		if (totalTripDuration == null) {
			if (other.totalTripDuration != null)
				return false;
		} else if (!totalTripDuration.equals(other.totalTripDuration))
			return false;
		if (transactionDate == null) {
			if (other.transactionDate != null)
				return false;
		} else if (!transactionDate.equals(other.transactionDate))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		return true;
	}

	/**
	 * When a mobile user edits an existing reservation, we should preserve original rez data
	 * @param db
	 * @param reservation
	 */
	public void copyReservationBasicInfo(Reservation db) {
		db.setLocations(this.getLocations());
		db.setNotes(this.getNotes());
		db.setPreferredVehicleType(this.getPreferredVehicleType());
		db.setRunType(this.getRunType());
		db.setPassengers(this.getPassengers());
		db.setPassengerCount(this.getPassengerCount());
		db.setSpecialInstructions(this.getSpecialInstructions());
		db.setActualChauffeurName(this.actualChauffeurName);
		db.setActualChauffeurNo(this.actualChauffeurNo);
		db.setActualVehicleNo(this.actualVehicleNo);
		db.setFees(this.getFees());
		
	}
	public void switchSubmissionTarget() {
		AffiliateReservation ar = this.getAffiliateReservation();
		if ( ar== null){
			return;
		}
		if (AffiliateReservation.SubmissionTarget.REQUESTER.equals(ar.getSubmissionTarget())){
			setGriddID(ar.getRequesterId());
			setResNo(ar.getRequesterResNo());
			ar.setSubmissionTarget(SubmissionTarget.PROVDIER);
		}else{
			setGriddID(ar.getProviderId());
			setResNo(ar.getProviderResNo());
			ar.setSubmissionTarget(SubmissionTarget.REQUESTER);
		}
		
	}
	public void applyConfirmationStatus(){
		AffiliateReservation af = this.getAffiliateReservation();
		if (af!=null ){
			if (ReservationStatus.BOOKING_CANCEL.equals(status)){
				af.setStatus(AffiliateReservationStatus.CANCEL_CONFIRMED);
			}else if (ReservationStatus.BOOKING_REJECT.equals(status)){
				af.setStatus(AffiliateReservationStatus.REJECTED);
			}else if (ReservationStatus.DISPATCH_COMPLETED.equals(status)){
				af.setStatus(AffiliateReservationStatus.RIDE_COMPLETED);
			}else if (ReservationStatus.DISPATCH_UN_ASSIGNED.equals(status)
					||ReservationStatus.DISPATCH_ASSIGNED.equals(status)
					||ReservationStatus.DISPATCH_ACKNOWLEDGED.equals(status)){
				af.setStatus(AffiliateReservationStatus.CONFIRMED);
			}else if (status.toString().indexOf("BOOKING")==0){
				af.setStatus(AffiliateReservationStatus.CONFIRMED);
			}else if (status.toString().indexOf("DISPATCH")==0){
				af.setStatus(AffiliateReservationStatus.IN_PROGRESS);
			}else if (status.toString().indexOf("ACCOUNT")==0)  {
				af.setStatus(AffiliateReservationStatus.RIDE_COMPLETED);
			}
		}
	}
	
	@Transient
	@JsonIgnore
	@XmlTransient
	public boolean  isStatusAllowedByPlatform(){
		// Chris Pond
		//Ideally the actualChauffeur property could be set, access to driver profile enabled and  a callback initiated on the Driver Confirmed status change in FastTrack (or Driver scheduled I suppose).
		// || ReservationStatus.DISPATCH_ACKNOWLEDGED.equals(this.status) )  {
		if (ReservationStatus.DISPATCH_ASSIGNED.equals(this.status)) {
			return false;
		}
		if (ReservationStatus.BOOKING_REJECT.equals(this.status) ||
			ReservationStatus.BOOKING_CANCEL.equals(this.status) ||
			ReservationStatus.BOOKING_REQUEST.equals(this.status) ||
			ReservationStatus.BOOKING_CONFIRMED.equals(this.status) ||
			ReservationStatus.DISPATCH_ACKNOWLEDGED.equals(this.status) || 
			ReservationStatus.DISPATCH_EN_ROUTE.equals(this.status) ||
			ReservationStatus.DISPATCH_ON_LOCATION.equals(this.status)||
			ReservationStatus.DISPATCH_PASSENGER_ON_BOARD.equals(this.status)||
			ReservationStatus.DISPATCH_NO_SHOW.equals(this.status)||
			ReservationStatus.DISPATCH_STOP.equals(this.status)||
			ReservationStatus.DISPATCH_COMPLETED.equals(this.status)||
			ReservationStatus.VENDOR.equals(this.status)){
			return true;
		}
		if (affiliateReservation!=null){
			
			if (AffiliateReservationStatus.CANCEL.equals(this.affiliateReservation.getStatus())||
				AffiliateReservationStatus.CANCEL_CONFIRMED.equals(this.affiliateReservation.getStatus())||
				AffiliateReservationStatus.CANCEL_REJECTED.equals(this.affiliateReservation.getStatus())||
				AffiliateReservationStatus.CONFIRMED.equals(this.affiliateReservation.getStatus())||
				AffiliateReservationStatus.IN_PROGRESS.equals(this.affiliateReservation.getStatus())){
				return true;
			}
		}
		
		return false;
	}
}
