/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.contact.Agent;
import net.grdd.common.domain.contact.Company;
import net.grdd.common.domain.contact.Operator;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * When Reservation will be handle between two affiliates.
 * Reservation object has uni-directional access to this object
 * 
 * @author rezashahbazi
 * @since Sep 12, 2011
 */
@Entity
@Table(name="reservation_affiliates")
public class AffiliateReservation extends BaseModel{

	private static final long serialVersionUID = -5702617582523310677L;
	// Resquesting (source) Affiliate, adapter usually sends the requesterId not fully loaded requester
	@Indexed
	private String requesterId;
	// Platform loads the requester operator and attaches to the AffiliateReservation
	private Company requester;
	
	// Reservation number in source	
	private String requesterResNo;

	// Agent can be a requester
	
	private Agent agent;

	// Affiliate how is suppose to handle the reservation
	@Indexed
	private String providerId;
	private Operator provider;
	
	// Reservation number in target side.
	private String providerResNo;
	
	private String providerResCode;
	
	private SubmissionTarget submissionTarget;  
	
	private String notes;

	private AffiliateReservationStatus status = AffiliateReservationStatus.NEW;

	// new, update, cancel
	private String action;
	
		
	public AffiliateReservation(){
		
	}
	
	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	//@NotNull
	public String getRequesterResNo() {
		return requesterResNo;
	}

	public void setRequesterResNo(String requesterResNo) {
		this.requesterResNo = requesterResNo;
	}
		
	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderResNo() {
		return providerResNo;
	}

	public void setProviderResNo(String providerResNo) {
		this.providerResNo = providerResNo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	@JsonIgnore
	@XmlTransient
	@Transient
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public Company getRequester() {
		return requester;
	}

	public void setRequester(Company requester) {
		this.requester = requester;
	}
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public Operator getProvider() {
		return provider;
	}

	public void setProvider(Operator provider) {
		this.provider = provider;
	}
	

	public AffiliateReservationStatus getStatus() {
		return status;
	}

	public void setStatus(AffiliateReservationStatus status) {
		this.status = status;
	}
		
	public String getProviderResCode() {
		return providerResCode;
	}

	public void setProviderResCode(String providerResCode) {
		this.providerResCode = providerResCode;
	}

	public SubmissionTarget getSubmissionTarget() {
		return submissionTarget;
	}

	public void setSubmissionTarget(SubmissionTarget submissionTarget) {
		this.submissionTarget = submissionTarget;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return "AffiliateReservation [requesterId=" + requesterId
				+ ", requesterResNo=" + requesterResNo + ", providerId="
				+ providerId + ", providerResNo=" + providerResNo
				+ ", providerResCode=" + providerResCode
				+ ", submissionTarget=" + submissionTarget + ", notes=" + notes
				+ ", status=" + status + "]";
	}


	public enum SubmissionTarget{
		REQUESTER, PROVDIER;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result
				+ ((providerId == null) ? 0 : providerId.hashCode());
		result = prime * result
				+ ((providerResNo == null) ? 0 : providerResNo.hashCode());
		result = prime * result
				+ ((requesterId == null) ? 0 : requesterId.hashCode());
		result = prime * result
				+ ((requesterResNo == null) ? 0 : requesterResNo.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime
				* result
				+ ((submissionTarget == null) ? 0 : submissionTarget.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AffiliateReservation other = (AffiliateReservation) obj;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (providerId == null) {
			if (other.providerId != null)
				return false;
		} else if (!providerId.equals(other.providerId))
			return false;
		if (providerResNo == null) {
			if (other.providerResNo != null)
				return false;
		} else if (!providerResNo.equals(other.providerResNo))
			return false;
		if (requesterId == null) {
			if (other.requesterId != null)
				return false;
		} else if (!requesterId.equals(other.requesterId))
			return false;
		if (requesterResNo == null) {
			if (other.requesterResNo != null)
				return false;
		} else if (!requesterResNo.equals(other.requesterResNo))
			return false;
		if (status != other.status)
			return false;
		if (submissionTarget != other.submissionTarget)
			return false;
		return true;
	}
	
}
