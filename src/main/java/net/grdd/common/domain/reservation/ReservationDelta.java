package net.grdd.common.domain.reservation;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.grdd.common.domain.DeltaAnnotation;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public  class ReservationDelta {

	final static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	static final Log LOG = LogFactory.getLog(ReservationDelta.class);

	String field;
	Object oldValue;
	Object newValue;
	
	final static String[] exclude = {"updateTime", "targetPlatform", "completedDate","serialVersionUID", "passengers"};
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public Object getOldValue() {
		return oldValue;
	}
	public void setOldValue(Object oldValue) {
		this.oldValue = oldValue;
	}
	public Object getNewValue() {
		return newValue;
	}
	public void setNewValue(Object newValue) {
		this.newValue = newValue;
	}
	public ReservationDelta() {
		super();
	}
	public ReservationDelta(String field, Object oldValue, Object newValue) {
		super();
		this.field = field;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	@Override
	public String toString() {
		return "[field=" + field + ", oldValue=" + oldValue
				+ ", newValue=" + newValue + "]";
	}
	public final static List<ReservationDelta> delta(Object newEntity,
			Object oldEntity) {
		
		List<ReservationDelta> changes = new ArrayList<ReservationDelta>();
		// If the entity is null or has no ID, it hasn't been persisted before,
		// so there's no delta to calculate
		if (newEntity == null || oldEntity == null) {
			LOG.warn("Tried to compare null objects - this is not allowed");
			return null;
		}

		
		// Calculate the difference
		// We need to fetch the fields from the parent entity as well as they
		// are not automatically fetched
		//Field[] fields = ArrayUtils.addAll(newEntity.getClass().getDeclaredFields(), newEntity.getClass().getDeclaredFields());
		List<Field> fields = new ArrayList<Field>();
        for (Class<?> c = newEntity.getClass(); c != null; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
		Object oldField = null;
		Object newField = null;
		//StringBuilder delta = new StringBuilder();
		for (Field field : fields) {
			field.setAccessible(true); // We need to access private fields
			try {
				oldField = field.get(oldEntity);
				newField = field.get(newEntity);
			} catch (IllegalArgumentException e) {
				LOG.warn("Bad argument given");
			
			} catch (IllegalAccessException e) {
				LOG.warn("Could not access the argument");				
			}
			if ((oldField != newField) && ! ArrayUtils.contains(exclude, field.getName())
					&& (((oldField != null) && !oldField.equals(newField)) || ((newField != null) && !newField
							.equals(oldField)))) {
				
				
				 
				 DeltaAnnotation annotation = field.getAnnotation(DeltaAnnotation.class);
				 if (annotation!=null){
					 continue;
				 }
				 ReservationDelta change = null;
				 if (field.getType().equals(Date.class)){
					 if (newField == null || oldField ==null ){
						 continue;
					 }
					 
					 if ( sdf.format(newField).equals(sdf.format(oldField))){
							continue;
					 }					 
				 }else if (field.getType().equals(String.class)){

					if (StringUtils.isBlank((String) newField) && StringUtils.isBlank((String) oldField)){
						continue;
					}
					
				}else if ( field.getType().getCanonicalName().contains("net.grdd") ){
					continue;					
				}
				 
				change =new ReservationDelta(field.getName(),oldField,newField);
				changes.add(change );
				
			}
		}
		if (newEntity.getClass().isAssignableFrom(Reservation.class)){
			Reservation newRes = (Reservation) newEntity;
			Reservation oldRes = (Reservation) oldEntity;
			if (newRes.getLocations()!=null && oldRes.getLocations()!=null){
				changes.addAll(delta(newRes.getLocations().getPickup(),oldRes.getLocations().getPickup()));
				changes.addAll(delta(newRes.getLocations().getDropOff(),oldRes.getLocations().getDropOff()));
				
			}
			if (newRes.getStatus()!=null && oldRes.getStatus()!=null){
				changes.addAll(delta(newRes.getStatus(), oldRes.getStatus()));
			}
			if (newRes.getAffiliateReservation()!=null && oldRes.getAffiliateReservation()!=null){
				changes.addAll(delta(newRes.getAffiliateReservation(), oldRes.getAffiliateReservation()));
			}
			if (newRes.getPrimaryPassenger()!=null && oldRes.getPrimaryPassenger()!=null){
				changes.addAll(delta(newRes.getPrimaryPassenger(), oldRes.getPrimaryPassenger()));
			}
			
			
		}

		// Persist the difference
		if (changes.isEmpty()) {
			LOG.debug("The delta is empty - this should not happen");
		} 
		return changes;
	}
	
	public static boolean isRideChangedDispatch(Reservation res1, Reservation res2){
		int result = new CompareToBuilder()	      
	       .append(res1.getResNo(), res2.getResNo())
	       .append(res1.getPrimaryPassenger(),res2.getPrimaryPassenger())
	       .append(res1.getPassengerCount(),res2.getPassengerCount())
	       .append(res1.getLocations().getPickup(), res2.getLocations().getPickup())
	       .append(res1.getLocations().getDropOff(), res2.getLocations().getDropOff())
	       .append(res1.getStatus(),res2.getStatus())
	       .append(res1.getRunType(),res2.getRunType())
	       .append(res1.getActualVehicleNo(),res2.getActualVehicleNo())
	       .append(res1.getActualChauffeurNo(),res2.getActualChauffeurNo())	  
	       .append(res1.getSpecialInstructions(),res2.getSpecialInstructions())	 
	       .append(res1.getReference(),res2.getReference())
	       .append(res1.getLocations().getStops(), res2.getLocations().getStops())
	       .toComparison();
		return result !=0;
	}
	public static boolean isRideChangedBook(Reservation res1, Reservation res2){
		boolean changed = isRideChangedDispatch( res1,  res2);
		if (changed) {
			return changed;
		}
		int result = new CompareToBuilder()	      
	       .append(res1.getBaseAmount(), res2.getBaseAmount())
	       .append(res1.getTotalAmount(),res2.getTotalAmount())
	       	       	       
	       .toComparison();
		return result !=0;
	}
	
	public static void main(String[] args) {
		Reservation res1 =  ReservationTestFactory.constructReservationWithDispatch();
		Reservation res2 =  ReservationTestFactory.constructReservationWithDispatch();
		
		List<ReservationDelta> delta = ReservationDelta.delta(res1,res2);
		System.out.println(delta);
	}
}