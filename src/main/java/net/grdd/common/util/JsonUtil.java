/*
 * Copyright (c) 2011-2012 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.util;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

/**
 * @author rezashahbazi
 * @since May 15, 2012
 */
public class JsonUtil {
	/**
	 * Convert the JSON array to collection of clazz type
	 * @param json
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	
	public static <T> List<T> mapJsonToObjectList(String json, Class clazz) throws Exception {
		List<T> list;
		ObjectMapper mapper = new ObjectMapper();
		TypeFactory t = TypeFactory.defaultInstance();
		list = mapper.readValue(json,
				t.constructCollectionType(ArrayList.class, clazz));
		return list;
	}
}
