package net.grdd.common.util;

import java.util.Date;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlTransient;

import net.grdd.common.domain.BaseModel;
import net.grdd.common.domain.reservation.AffiliateReservation;
import net.grdd.common.domain.reservation.Reservation;

import org.bson.types.ObjectId;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="integration-log")
@JsonSerialize(include = Inclusion.NON_NULL)
public class IntegrationLog extends BaseModel{
	
	private static final long serialVersionUID = 1L;
	
	  
    private String vendorPlatform; // LA, Adapter, Minires
    private String status ;
    private String driver;
    private String message;
    private Reservation reservation;
    
    @Id
	@Column(name="_id")
	@JsonIgnore
	@XmlTransient
	private ObjectId id;
    
    public IntegrationLog(){
    	//DateTime now = new DateTime(DateTimeZone.UTC);
		//reservation.setUpdateTime(now.toDate());		
    	this.setCreateDate(new Date());
    }

    /**
     * use reservation
     */
    @Deprecated
    private AffiliateReservation request;
    
    private Object response;
    
    /**
     * Stores vendor's last update date
     */
    private Date vendorUpdateTime;
    
	
	public String getVendorPlatform() {
		return vendorPlatform;
	}
	public void setVendorPlatform(String vendorPlatform) {
		this.vendorPlatform = vendorPlatform;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public AffiliateReservation getRequest() {
		return request;
	}
	public void setRequest(AffiliateReservation request) {
		this.request = request;
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	
	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	
	public Date getVendorUpdateTime() {
		return vendorUpdateTime;
	}
	public void setVendorUpdateTime(Date vendorUpdateTime) {
		this.vendorUpdateTime = vendorUpdateTime;
	}
	
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	@Override
	public String toString() {
		return "IntegrationLog [status=" + status + 
				", vendorPlatform=" + vendorPlatform
				+ ", message=" + message + ", reservation=" + reservation
				+ ", response=" + response + ", vendorUpdateTime="
				+ vendorUpdateTime + "]";
	}
	
	
	
    
}
