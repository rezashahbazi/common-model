/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.dispatch;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Common Dispatch Exception class. 
 * @author rezashahbazi
 * @since Oct 18, 2011
 */
@JsonIgnoreProperties({"stackTrace"})
public class DispatchException extends Exception {

	private static final long serialVersionUID = 6186895311919699429L;

	public DispatchException() {
		super();
	}

	public DispatchException(String message) {
		super(message);
	}
	
	public DispatchException(Throwable throwable) {
		super(throwable);
	}
}
