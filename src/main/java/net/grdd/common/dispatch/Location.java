package net.grdd.common.dispatch;

import org.springframework.data.mongodb.core.mapping.Field;

public class Location{
		private float lon;
		private float lat;
		@Field(order = 1)
		public float getLon() {
			return lon;
		}
		public void setLon(float lon) {
			this.lon = lon;
		}
		@Field(order = 2)
		public float getLat() {
			return lat;
		}
		public void setLat(float lat) {
			this.lat = lat;
		}
		public Location() {
			
		}
		public Location(float lon, float lat) {
			this.lon = lon;
			this.lat = lat;
		}
		
		
		
	}