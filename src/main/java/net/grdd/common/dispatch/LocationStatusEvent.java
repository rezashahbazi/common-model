/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */

package net.grdd.common.dispatch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import net.grdd.common.domain.DateDeserializer;
import net.grdd.common.domain.DateSerializer;
import net.grdd.common.domain.vehicle.Vehicle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Inbound message from clients.  This is a union of various states
 * to make marshalling easier.
 * 20 minutes
 * db.chfcurloc.ensureIndex( { "createDate": 1 }, { expireAfterSeconds: 1200 } )
 * db.chfcurloc.ensureIndex( { "griddID": 1 })
 * db.chfcurloc.ensureIndex( { "location" : "2d" } )
 * @author paulli
 * @since Oct 6, 2011
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
@Document(collection="chfcurloc")
public class LocationStatusEvent
	implements Serializable
{
	private static final long serialVersionUID = 1L;

	
	
	private Date timestamp;		// from client
	private Date createDate = new Date() ;

	private String code;		// from client, TRIP_*, CHF_*
	private String griddID;
	private String chfName;	
	private String chfId;
	@Indexed
	private String transactionId;
	private Location location = new Location();

	/** If driver is in onDemand pool */
	private Boolean onDemand;
	/** If driver status is not in dispatch */
	private Boolean available;
	
	private Vehicle vehicle;
	
	
	public LocationStatusEvent() {
				
	}
	public LocationStatusEvent(float lon, float lat) {
	
		this.location = new Location( lon,  lat);
	}
	public LocationStatusEvent(float lon, float lat, Date timestamp) {
		this.setTimestamp(timestamp);
		this.location = new Location( lon,  lat);
	}

	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getTimestamp()
	{
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp)
	{
		this.timestamp = timestamp;
	}

	
	
	

	public void setLat(BigDecimal lat)
	{
		this.location.setLat(lat!=null?lat.floatValue():0);
	}

	@JsonSerialize(using=DateSerializer.class,include = Inclusion.NON_NULL)
	@JsonDeserialize(using=DateDeserializer.class)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setLon(BigDecimal lon)
	{
		this.location.setLon(lon!=null?lon.floatValue():0);
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}
	

	public String getGriddID() {
		return griddID;
	}

	public void setGriddID(String griddID) {
		this.griddID = griddID;
	}

	public void setOperatorId(String operatorId) {
		this.griddID = operatorId;
	}

	public String getChfName() {
		return chfName;
	}
	
	public void setChfName(String chfName) {
		this.chfName = chfName;
	}

	public String getChfId() {
		return chfId;
	}

	public void setChfId(String chfId) {
		this.chfId = chfId;
	}
	
	

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
		

	public Boolean getOnDemand() {
		return onDemand;
	}

	public void setOnDemand(Boolean onDemand) {
		this.onDemand = onDemand;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
	
}
