/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.services;

import java.math.BigDecimal;

import net.grdd.common.domain.contact.Client;
import net.grdd.common.domain.contact.Company;
import net.grdd.common.domain.contact.Operator;
/**
 * Generic Operator Service define operations like get operator for given griddID, etc
 * @author rezashahbazi
 * @since Jun 1, 2012
 */
public interface AffiliateService {
	/**
	 * Calls Affiliate Service and get the operator by operator id
	 * @param griddID
	 * @return
	 */
	public Operator getOperator(String griddID) ;
	
	public Client getClient(String griddID) ;
	
	/**
	 * Gets different combinations of company including operator, client and agent.
	 * @param griddID
	 * @deprecated use getComapny
	 * @return
	 */
	@Deprecated
	public Company getAffiliate(String griddID) ;
	
	public Company getCompany(String griddID) ;
	
	
	/**
	 * If griddID1 and griddID2 has already established ACTIVE affiliation
	 * @param griddID1 griddId of either a client or an operator
	 * @param griddID1 
	 * @return true if they are active
	 */
	public boolean affiliateCheck(String griddID1, String griddID2) ;

	/**
	 * Get Currency symbol for given country name
	 * @param country
	 * @return Symbol like CAN, default $
	 */
	public String getCurrencySymbole(String country);
	
	BigDecimal getAffiliateTaxRate(String griddID);
}
