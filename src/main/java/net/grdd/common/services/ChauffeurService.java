/*
 * Copyright (c) 2011-2012 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.services;

import java.util.List;

import net.grdd.common.domain.contact.chauffeur.Chauffeur;
import net.grdd.common.domain.vehicle.Vehicle;

/**
 * Call Chauffeur Service and invoke the operations or retrieve documents
 * @author rezashahbazi
 * @since Apr 25, 2012
 */
public interface ChauffeurService {
	/**
	 * Retrieve Chauffeur by id
	 * @param griddID: operator id
	 * @param chauffeurId
	 * @return
	 */
	public Chauffeur getByChauffeurId(String griddID, String chauffeurId);
	
	/**
	 * List of active Chauffeurs 
	 * @param griddID
	 * @return
	 */
	public List<Chauffeur> findByOperator(String griddID);
	
	public Chauffeur findByEmail(String email);
	/**
	 * List of active vehicles
	 * @param griddID
	 * @return
	 */
	public List<Vehicle> getActiveVehicles(String griddID);
	public Vehicle getVehicle(String griddID, String vehicleId);
}


