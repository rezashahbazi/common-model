package net.grdd.common.domain;

import java.util.Date;

import org.junit.Test;

public class LocalDateTimeUtilTest {

	@Test
	public void testConvertTimeZoneFromUTC() {

		System.out.println("PST "+ LocalDateTimeUtil.INSTANCE.convertTimeZoneFromUTC(new Date(),"yyyy-MM-dd H:mm:ss", "PST"));
		System.out.println("America/Los_Angeles "+ LocalDateTimeUtil.INSTANCE.convertTimeZoneFromUTC(new Date(),"yyyy-MM-dd H:mm:ss", "America/Los_Angeles"));
		
		System.out.println("null " +LocalDateTimeUtil.INSTANCE.convertTimeZoneFromUTC(new Date(),"yyyy-MM-dd H:mm:ss", null));
		System.out.println("+3.5 " +LocalDateTimeUtil.INSTANCE.convertTimeZoneFromUTC(new Date(),"yyyy-MM-dd H:mm:ss", "+3.5"));
		System.out.println("-7 " + LocalDateTimeUtil.INSTANCE.convertTimeZoneFromUTC(new Date(),"yyyy-MM-dd H:mm:ss", "-7"));
	}

}
