package net.grdd.common.domain;

import junit.framework.Assert;

import org.junit.Test;

public class AddressTest {

	@Test
	public void testHasStandardAddress() {
		Address address = new Address();
		address.setAddress("123 Avenue of art, philadelphia pa 12345");
		Assert.assertFalse(address.hasStandardAddress());
		
		address.setState("PA");
		Assert.assertFalse(address.hasStandardAddress());
		
		address.setCity("philadelphia");
		Assert.assertTrue(address.hasStandardAddress());
	}

	@Test
	public void testIsStandard() {
		Address address = new Address();
		Assert.assertFalse(address.isXAL());
		address.setAddress("123 Avenue of art, philadelphia pa 12345");
		Assert.assertTrue(address.isXAL());
		
		address.setCity("philadelphia");
		Assert.assertTrue(address.isXAL());
	}

}
