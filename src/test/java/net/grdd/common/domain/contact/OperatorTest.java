package net.grdd.common.domain.contact;

import java.util.List;
import java.util.Map;

import net.grdd.common.domain.vehicle.VehicleType;

import org.junit.Assert;
import org.junit.Test;

public class OperatorTest {
	Operator operator = new Operator();
	@Test
	public void testGetMappedVehicleTypes() {
		//"[{"type":"STRETCH","value":"3"},{"type":"SEDAN","value":"1"},{"type":"SUV","value":"2"}]"
		operator.addConfiguration("vehicle_types", "[{\"type\":\"STRETCH\",\"value\":\"3\"},{\"type\":\"SEDAN\",\"value\":\"1\"},{\"type\":\"SUV\",\"value\":\"2\"}]");
		List<VehicleType> mappedVehicleTypes = operator.getMappedVehicleTypes();
		Assert.assertNotNull(mappedVehicleTypes);
		Assert.assertTrue(mappedVehicleTypes.size()==3);
	}
	@Test
	public void testGetMappedVehicleTypesAsMap() {
		//"[{"type":"STRETCH","value":"3"},{"type":"SEDAN","value":"1"},{"type":"SUV","value":"2"}]"
		operator.addConfiguration("vehicle_types", "[{\"type\":\"STRETCH\",\"value\":\"3\"},{\"type\":\"SEDAN\",\"value\":\"1\"},{\"type\":\"SUV\",\"value\":\"2\"}]");
		Map<String, String> map = operator.getMappedVehicleTypesAsMap();
		Assert.assertNotNull(map);
		Assert.assertTrue(map.size()==3);
	}

}
