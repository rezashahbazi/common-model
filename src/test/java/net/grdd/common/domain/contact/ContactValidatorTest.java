package net.grdd.common.domain.contact;

import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

public class ContactValidatorTest {
	private LocalValidatorFactoryBean validator;

	@Before
	public void setup() {
		validator = new ContactValidator();
		validator.setProviderClass(HibernateValidator.class);
		validator.afterPropertiesSet();
	}
	
	@Test
	public void testValidateObjectErrorsObjectArray() {
		Company c = new Company();
		Errors errors = new BeanPropertyBindingResult(c,"Company");
		validator.validate(c, errors );
	}

}
