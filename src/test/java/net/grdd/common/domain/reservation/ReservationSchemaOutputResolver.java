package net.grdd.common.domain.reservation;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

public class ReservationSchemaOutputResolver extends SchemaOutputResolver{
	public Result createOutput(String namespaceURI, String suggestedFileName) throws IOException {
        File file = new File("src/test/java/reservation.xsd");      
        StreamResult result = new StreamResult(file);
        result.setSystemId(file.toURI().toURL().toString());
        System.out.println("XSD file generated :"+ file.getAbsolutePath());
        return result;
    }
}
