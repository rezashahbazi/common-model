/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.io.IOException;
import java.io.InputStreamReader;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ParseReservationTest {
	private static JsonFactory jf = new JsonFactory();
	ObjectMapper mapper = new ObjectMapper();
	
	@Test 
	public void readJSON() throws IOException {		
		Reservation res = mapper.readValue(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("net/grdd/common/domain/reservation/res.json")), Reservation.class);
		System.out.println(res);
		//Reservation res = mapper.readValue(new File("net/grdd/common/domain/reservation/res.json"), Reservation.class);
		
	}
	
}
