/*
 * Copyright (c) 2011 GRiDD Technologies, CA INC.
 * 11054 Ventura Blvd #281 Studio City, CA 91604
 * (818) 724-7433
 * 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of GRiDD ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with GRiDD.
 */
package net.grdd.common.domain.reservation;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReservationTest {
	private static JsonFactory jf = new JsonFactory();
	ObjectMapper mapper = new ObjectMapper();
	Reservation reservation = new Reservation();

	@Before
	public void setup() {
		reservation = ReservationTestFactory.constructReservation();
		//mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
	}

	//@Test 
	public void makeJSON() throws IOException {

		StringWriter sw = new StringWriter();
		JsonGenerator jg = jf.createJsonGenerator(sw);

		jg.useDefaultPrettyPrinter();
		
		//DateFormat df= new SimpleDateFormat("dd/MM/yy");
		//mapper.getSerializationConfig().withDateFormat(df);
		//mapper.getSerializationConfig().setDateFormat(df);
		mapper.writeValue(jg, reservation);		
	
		System.out.println("Simple Reservatoin:\n"+sw.toString());
	}
	@Test
	public void makeWithDispatchJSON() throws IOException {
		reservation = ReservationTestFactory.constructReservationWithDispatch();
		StringWriter sw = new StringWriter();
		JsonGenerator jg = jf.createJsonGenerator(sw);

		jg.useDefaultPrettyPrinter();
		
		
		mapper.writeValue(jg, reservation);		
	
		System.out.println(sw.toString());
	}
	
	@Test
	public void makeWithDispatchFarmout() throws IOException {
		Reservation farmin = ReservationTestFactory.constructReservationWithFarmin();
		farmin.setUpdateTime(new Date());
		StringWriter sw = new StringWriter();
		JsonGenerator jg = jf.createJsonGenerator(sw);

		jg.useDefaultPrettyPrinter();
		
		
		mapper.writeValue(jg, farmin);		
	
		System.out.println("Farmin:\n"+sw.toString());
	}
	//@Test
	public void testActiveStatus(){
		
		Assert.assertTrue(reservation.isActive());
		
		reservation=ReservationTestFactory.constructReservationWithFarmin();
		reservation.setStatus(null);
		reservation.getAffiliateReservation().setStatus(AffiliateReservationStatus.CONFIRMED);
		Assert.assertFalse(reservation.isActive());
		
		reservation.setStatus(ReservationStatus.ACCOUNT_BILLED);
		Assert.assertFalse(reservation.isActive());
	}
	
	@Test
	public void testIsLocal(){
		
		Assert.assertTrue(reservation.isLocal());
		AffiliateReservation affiliateReservation = new AffiliateReservation();
		reservation.setAffiliateReservation(affiliateReservation );
		Assert.assertTrue(reservation.isLocal());
		
		affiliateReservation.setProviderId("Sandbox");
		Assert.assertTrue(reservation.isLocal());
		
		affiliateReservation.setRequesterId("Sandbox");
		Assert.assertTrue(reservation.isLocal());
		
		reservation.setGriddID("Diva");
		Assert.assertFalse(reservation.isLocal());
		
		reservation.setGriddID("Sandbox");
		affiliateReservation.setRequesterId("Foxy");
		Assert.assertFalse(reservation.isLocal());
		
		
	}
	
	@Test
	public void testIsProvider(){
		Assert.assertTrue(reservation.isProvider());
		
		AffiliateReservation affiliateReservation = new AffiliateReservation();
		reservation.setAffiliateReservation(affiliateReservation );
		Assert.assertTrue(reservation.isProvider());
		
		affiliateReservation.setProviderId("Sandbox");
		Assert.assertTrue(reservation.isProvider());
		reservation.setGriddID("Diva");
		Assert.assertFalse(reservation.isProvider());
		
		
		reservation.setGriddID("Sandbox");
		affiliateReservation.setRequesterId("Foxy");
		Assert.assertTrue(reservation.isProvider());
	}
	
	@Test
	public void testIsComplete(){
		reservation.setStatus(ReservationStatus.DISPATCH_COMPLETED);
		reservation.isComplete();
	}
}
