package net.grdd.common.domain.reservation;

import org.junit.Assert;
import org.junit.Test;

public class LiveryLocationTest {

	@Test
	public void testGetShortLocation() {
		LiveryLocation loc =  new LiveryLocation();
		loc.setAddress("123 Santa Monica Boulevard, Santa Monica, CA");
		System.out.println(loc.getShortLocation());
		Assert.assertNotNull(loc.getShortLocation());
		
		loc.setAddress2("#34");
		Assert.assertEquals("#34 123 Santa Monica Boulevard, Santa Monica, CA",loc.getShortLocation());
		System.out.println(loc.getShortLocation());
		
		loc.setAddress1("123 Santa Monica");
		System.out.println(loc.getShortLocation());
		
		loc.setLandmark("LAX");
		System.out.println(loc.getShortLocation());
		Assert.assertEquals("LAX 123 Santa Monica  #34",loc.getShortLocation());
		
	}

}
