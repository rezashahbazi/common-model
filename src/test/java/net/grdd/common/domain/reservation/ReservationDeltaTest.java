package net.grdd.common.domain.reservation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ReservationDeltaTest {

	@Test
	public void testDelta() {
		List<ReservationDelta> delta = ReservationDelta.delta(new Reservation(), new Reservation());
		Assert.assertTrue(delta.size()==0);
	}

	@Test
	public void testDeepDelta() {
		Reservation res1 =  ReservationTestFactory.constructReservationWithDispatch();
		Reservation res2 =  ReservationTestFactory.constructReservationWithDispatch();
		List<ReservationDelta> delta = ReservationDelta.delta(res1,res2);
		System.out.println(delta);
		res1.getLocations().getPickup().setTime(new Date(123423232));
		res1.setTotalAmount(new BigDecimal(203));
		res1.getPrimaryPassenger().setFirstName("Alex");
		delta = ReservationDelta.delta(res1,res2);
		Assert.assertNotNull(delta);
		System.out.println(delta);
	}
	
	@Test
	public void  isRideChange(){
		Reservation res1 =  ReservationTestFactory.constructReservationWithDispatch();
		Reservation res2 =  ReservationTestFactory.constructReservationWithDispatch();
		boolean rideChangedBook = ReservationDelta.isRideChangedBook(res1, res2);
		System.out.println(rideChangedBook);
	}

}
