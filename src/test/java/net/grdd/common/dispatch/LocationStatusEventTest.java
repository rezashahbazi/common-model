package net.grdd.common.dispatch;

import java.io.StringWriter;
import java.util.Date;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

public class LocationStatusEventTest {
	private static JsonFactory jf = new JsonFactory();
	ObjectMapper mapper = new ObjectMapper();
	@Test
	public void testSimpleLocation() throws Exception {
		LocationStatusEvent location = new LocationStatusEvent();
		location.setChfId("Amir");
		location.setChfName("Amir Zafar");
		location.setGriddID("Sandbox");
		
		location.setTimestamp(new Date());
		
		StringWriter sw = new StringWriter();
		JsonGenerator jg = jf.createJsonGenerator(sw);

		jg.useDefaultPrettyPrinter();
		
		
		mapper.writeValue(jg, location);		
	
		System.out.println("Location:\n"+sw.toString());
	}

}
